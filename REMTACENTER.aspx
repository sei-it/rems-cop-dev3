﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsForumMasterPage.master" AutoEventWireup="true" CodeFile="REMTACENTER.aspx.cs" Inherits="REMTACENTER" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">

<div class="remsCOP-content">
    <div class="rcontainer">
    <div class="container ad">
 <div class="grid_12 omega"> 
 <h2>We’ve Joined Twitter! Follow us <a href="https://twitter.com/remstacenter">@REMTACENTER</a></h2>
 <div class="grid_12 omega">
<span class="fa fa-twitter-square blue"></span>
 <p>This back-to-school season marks many fresh starts and new beginnings for the REMS TA Center. Not only have we updated our site and several tools, including EOP ASSIST and EOP EVALUATE, but we’ve also joined the Twitter family! You can now visit us at @remstacenter to get up-to-date access to our site updates, tool launches, publication releases, training opportunities, and much more. If you’re already on Twitter, please feel free to send us your Twitter handle so that we can connect with you there. We look forward to the chance to expand our virtual relationship. </p>
 <div class="grid_12 omega"> 
<p>Sincerely,
  <br>
  The REMS TA Center
  <br>
  Twitter: <a href="https://twitter.com/remstacenter">@remstacenter</a>
  <br>
  Email: <a href="mailto:info@remstacenter.org">info@remstacenter.org</a>
  <br>
  Phone: 1-855-781-REMS [7367] </p>
 </div>
 </div>

 </div>
 </div>
 </div>
 </div>

</asp:Content>

