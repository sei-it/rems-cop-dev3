﻿<%@ Page Title="" Language="C#" MasterPageFile="~/REMSCOPforum/remsForumMasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentLeftMenu" Runat="Server">
    <div class="column left forumleft">
<h4 class="sideMenu">Community of Practice</h4>
<ul>
<li><asp:Label ID="lblid1" Width="225" ForeColor="#85952A" runat="server"><strong>Home</strong></asp:Label></li>
<li><asp:LinkButton ID="LinkButton2" PostBackUrl="~/2_About.aspx" runat="server">About the REMS TA Center CoP</asp:LinkButton></li>
<%--<li><asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/AboutTeam.aspx">Meet the REMS TA Center Team</asp:LinkButton></li>--%>
<li><asp:LinkButton ID="LinkButton4" PostBackUrl="~/3_PortalRules.aspx" runat="server">Community Rules</asp:LinkButton></li>
<li><asp:LinkButton ID="LinkButton3" PostBackUrl="~/Account/Register.aspx" runat="server">Join the Community</asp:LinkButton></li>
<li><asp:LinkButton ID="LinkButton9" runat="server" PostBackUrl="~/REMSCOPforum/Default.aspx">Community Forums</asp:LinkButton></li>
<li><asp:LinkButton ID="lbtnloginout" PostBackUrl="~/Account/Login.aspx?ReturnUrl=../REMSCOPforum/default.aspx" runat="server" Text="Log in"></asp:LinkButton></li>
<%--<li><asp:LinkButton ID="LinkButton1" onClientClick="window.open('events.aspx');" runat="server">Events</asp:LinkButton></li>--%>
</ul>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
    <div class="maincolumn">
   <%-- <h1>
        Welcome to the REMS TA Center Community of Practice (CoP) Portal
    </h1>--%>
    <!--<div id="cop_bottomlinks">   
<div id="cb_lowerrow">
	<asp:ImageButton ID="ibtnJoin" ImageUrl="~/images/cop_join.png"  runat="server" />
	<asp:ImageButton ID="ibtnstartgroup" ImageUrl="~/images/cop_start.png" runat="server" ToolTip="Groups are designed to help specific subgroups of community members come together to participate in more focused discussions, collaborate on special projects, or streamline access to local content and conversations, by invitation or request." />
	<asp:ImageButton ID="ibtnenterforum" ImageUrl="~/images/cop_enter.png" runat="server" ToolTip="Forums are designed to facilitate discussions among all community members about common questions, issues, and trends related to emergency management and developing EOPs." Text="ENTER A FORUM" />
    <asp:ImageButton ID="ibtmyprofile" ImageUrl="~/images/cop_profile.png" runat="server"  />
    </div>
    <div id="lm"><asp:ImageButton ID="ibtnLm" ImageUrl="~/images/cop_learnmore.png" PostBackUrl="~/2_About.aspx" runat="server" /></div>
</div>-->

    <div id="whatis">
    	<h2 class="yellow">What is the REMS TA Center CoP? </h2>
        <div class="yelarr"><img src="images/yellow_arrowDOWN.png"></div>
  <p>A virtual space for schools, school districts, institutions of higher education (IHEs), and their community partners to collaborate on special projects, share news and resources, discuss trends and ideas, and learn from the experiences of others in the field.</p>
      
    </div>
    <div id="whatis">
    	<h2 class="yellow">Who Should Join?</h2>
        <div class="yelarr"><img src="images/yellow_arrowDOWN.png"></div>
        <table class="data-table">
    <tr>
        <th class="border-bottom">K-12 Schools and School Districts</th>
        <th class="border-bottom">Institutions of Higher Education</th>
    </tr>
    <tr>
        <td>Practitioners Addressing Emergency Management</td>
        <td>Practitioners Addressing Emergency Management</td>
    </tr>
    <tr>
        <td>Administrators, Educators, Principals, or Senior-Level Officials</td>
        <td>Presidents, Chancellors, or Provosts</td>
    </tr>
    <tr>
        <td>School Resource Officers and City or County Law Enforcement Personnel</td>
        <td>Campus Police and City or County Law Enforcement Personnel</td>
    </tr>
    <tr>
        <td>First Responders</td>
        <td>First Responders</td>
    </tr>
    <tr>
        <td>Mental or Public Health Professionals and School Nurses</td>
        <td>Mental or Public Health Professionals and Campus Health Center Representatives</td>
    </tr>
    <tr>
        <td>Parents or Guardians</td>
        <td>Family Members</td>
    </tr>
</table>
        <!--
                <ul>
                  <li>Seasoned Practitioners Addressing Emergency Management at Schools, Districts or IHEs</li>
                  <li>K-12 Administrators, Educators, Principals, or Senior-Level Officials</li>
                  <li>IHE Presidents, Chancellors, or Provosts</li>
                  <li>School Resource Officers or Law Enforcement Officers Working With a School, District, or IHE</li>
                  <li>First Responders</li>
                  <li>Counselors or Psychologists</li>
                  <li>Parents</li>
                </ul> -->
    </div>
    <div id="whyjoin">
    	<h2 class="yellow">How Can You Contribute and Connect?</h2>
        <div class="yelarr"><img src="images/yellow_arrowDOWN.png"></div>
        
    <ul class="whyjoin">
        <li>Share lessons learned, see what other schools and IHEs are doing, and ask questions.</li>
        <li>View resources, ask questions, share your concerns, and contribute ideas to improve emergency operations plan (EOP) development and processes before, during, and after emergency events.</li>
        <li>Share your experiences, discuss processes and regulations, and offer advice to others in the field.</li>
        <li>Share success stories and safety tips based on real-world experiences with school emergencies.</li>
        <li>Offer recommendations for psychological recovery efforts.</li>
    </ul>
    
    </div>
    <div id="coplogin">
    	<ul class="coplogin">
        	<li><a href="Account/Register.aspx" title="" class="btnCop">Join</a></li>
           <%--<li><a href="#" title="" class="btnCop">Start A Group</a></li>--%>
           <li><a href="REMSCOPforum/default.aspx" title="" class="btnCop">Enter A Forum</a></li>
           <li><a href="REMSCOPforum/editprofile.aspx" title="" class="btnCop">My Profile</a></li>
           <li class="lastli"><a href="Account/Login.aspx" title="" class="btnCop"><asp:Literal ID="ltlLoginout" runat="server" Text="Log in" /></a></li>
        </ul>
        <div class="clear"></div>
    </div>



</div>
</asp:Content>

