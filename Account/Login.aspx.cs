﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Text;
using System.Net.Mail;

public partial class Account_Login : System.Web.UI.Page
{
    remsUsersDataClassesDataContext db = new remsUsersDataClassesDataContext();
    protected void page_init(object sender, EventArgs e)
    {
        TextBox txtUserName = LoginUser.FindControl("UserName") as TextBox;
        TextBox txtPassword = LoginUser.FindControl("Password") as TextBox;
        txtUserName.Text = "";
        txtPassword.Text = "";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ValdiateUser();
    }

    private void ValdiateUser()
    {
        string uid = Session["aspnetforumUserID"] == null ? "" : Session["aspnetforumUserID"].ToString();
        string uName = Session["aspnetforumUserName"]==null?"": Session["aspnetforumUserName"].ToString();
        if (!string.IsNullOrEmpty(uid))
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect( "~/default.aspx");
            return;
        }

        TextBox txtUserName = LoginUser.FindControl("UserName") as TextBox;
        TextBox txtPassword = LoginUser.FindControl("Password") as TextBox;

        if (Membership.ValidateUser(txtUserName.Text.Trim(), txtPassword.Text))
        {
            String sPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(txtPassword.Text, "md5");

            FormsAuthentication.SetAuthCookie(txtUserName.Text, true);

            if (Request.Params["ReturnUrl"] != null)
            {
                RegisterHyperLink.NavigateUrl = "Register.aspx?ReturnUrl=" + HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            }
            else
                LoginUser.DestinationPageUrl= "~/REMSCOPforum/COPindex.aspx";

        }
        
    }

    protected void btnLostPassword_Click(object sender, EventArgs e)
    {
        LoginView.Visible = false;
        RecoverView.Visible = true;
        ResetView.Visible = false;
        lblError.Visible = false;
    }

    protected void Recover_Click(object sender, EventArgs e)
    {
        lblError.Visible = false;
        try
        {
            if (LostUserName.Text.Length == 0)
            {
                lblError.Visible = true;
                lblError.Text = "User name is required.";
                return;
            }
            else if (txtEmail.Text.Trim().Length == 0)
            {
                lblError.Visible = true;
                lblError.Text = "Email is required.";
                return;
            }
            string oldPassword = "";
            MembershipUser user = Membership.GetUser(LostUserName.Text);
            if (user != null)
            {
                oldPassword = user.ResetPassword();

                if (user.Email.ToString().Trim().ToLower() != txtEmail.Text.Trim().ToLower())
                {
                    lblError.Visible = true;
                    lblError.Text = "We were unable to create your new password.<br/> Please make sure you have entered a valid e-mail.";
                    return;
                }

                //send email
                var profile = db.remsUsers.SingleOrDefault(x => x.aspnet_UserName == user.UserName);
                string userfullname = profile == null ? user.UserName : profile.FirstName + " " + profile.LastName;
                string strFrom = " info@remstacenter.org";
                MailMessage objMailMsg = new MailMessage(strFrom, user.Email);

                objMailMsg.BodyEncoding = Encoding.UTF8;
                objMailMsg.Subject = "REMS TA Center: Community of Practice Account Information";
                //objMailMsg.Body = "<p>Dear " + LostUserName.Text + ",</p><p>As requested, here is your new password to log into the Community of Practice private workspace. The next time you log in, click “Change Password” below the username and password fields, and enter your temporary password in the “Old Password” field.</p><p>Temporary password: "
                //+ oldPassword + "</p>";
                objMailMsg.Body = "<p>Dear " + userfullname + ",</p><p><b>Thank you for registering for the Readiness and Emergency Management for Schools (REMS) Technical Assistance (TA) Center Community of Practice. Please use the information below to login to your account:</b></p>";
                objMailMsg.Body += "<p>User Name (Email Address): " + user.UserName + "<br/>";
                objMailMsg.Body += "Password: " + oldPassword + "</p>";
                objMailMsg.Body += "<b><p>If you require technical support accessing the Community of Practice, please email info@remstacenter.org.</p>Sincerely,<br/>The REMS TA Center Team</b>";


                objMailMsg.Priority = MailPriority.High;
                objMailMsg.IsBodyHtml = true;

                //prepare to send mail via SMTP transport
                SmtpClient objSMTPClient = new SmtpClient();
                objSMTPClient.Host = "mail2.seiservices.com";
                //NetworkCredential userCredential = new NetworkCredential("SEInfo@seiservices.com", "");
                //objSMTPClient.Credentials = CredentialCache.DefaultNetworkCredentials;
                objSMTPClient.Send(objMailMsg);
                LoginView.Visible = true;
                RecoverView.Visible = false;

                ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmation", "<script>alert('A temporary password has been e-mailed to you.');</script>", false);
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = "We were unable to create your new password.<br/>";
                lblError.Text += " Please make sure you have entered a valid username.";
            }
        }
        catch (Exception x)
        {
            //ILog Log = LogManager.GetLogger("EventLog");
            //Log.Fatal("Recover password.", x);
            lblError.Visible = true;
            lblError.Text = "We were unable to create your new password.<br/>Please contact your administrator.";
            //lblError.Text += x.ToString();
        }
    }

    protected void btnChangePassword_Click(object sender, EventArgs e)
    {
        LoginView.Visible = false;
        RecoverView.Visible = false;
        ResetView.Visible = true;
        lblError.Visible = false;
    }
    protected void OnChangePassword(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            MembershipUser user = Membership.GetUser(txtResetUserName.Text);
            try
            {
                if (user != null && !string.IsNullOrEmpty(txtOldPassword.Text) && !string.IsNullOrEmpty(txtNewPassword.Text))
                {
                    if (user.ChangePassword(txtOldPassword.Text, txtNewPassword.Text))
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmation", "<script>alert('Your password has been successfully changed!');</script>", false);
                        LoginView.Visible = true;
                        RecoverView.Visible = false;
                        ResetView.Visible = false;
                        lblError.Visible = false;
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmation", "<script>alert('We were unable to change your password. Please ensure that you have entered the correct username and old password, and that your new password contains at least 6 characters.');</script>", false);
                }
                else
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmation", "<script>alert('We were unable to change your password. Please ensure that you have entered the correct username and old password, and that your new password contains at least 6 characters.');</script>", false);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmation", "<script>alert('We were unable to change your password. Please ensure that you have entered the correct username and old password, and that your new password contains at least 6 characters.');</script>", false);
            }
        }
    }
}
