﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/remsForumMasterPage.master" AutoEventWireup="true"
    CodeFile="Register.aspx.cs" Inherits="Account_Register" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <script language="javascript" type="text/javascript">
    function ValidateChkList(source, arguments) {
        var validateID = source.id.toString().split('_')[3];
        arguments.IsValid = IsCheckBoxChecked(validateID) ? true : false;
        if (validateID == "CustomValidator2" || validateID == "CustomValidator3") {
            var otherDes = $("#<%= txtOtherRoleDes.ClientID %>").val();

            if (otherDes.trim() != "")
                arguments.IsValid = true;

        }
    }

  
    function IsCheckBoxChecked(vid) {
        var isChecked = false;
        var list = null;
        var typelist = document.getElementById('<%= cblInstType.ClientID %>');
        if (vid == "CustomValidator1")
            list = typelist;
        else if (vid == "CustomValidator2")
            list = document.getElementById('<%= rblK12Role.ClientID %>');
        else if (vid == "CustomValidator3")
            list = document.getElementById('<%= rblHigherRole.ClientID %>');
        else if (vid == "CustomValidator4")
            list = document.getElementById('<%= cblInterests.ClientID %>');
        else if (vid == "CustomValidator5") {
            var accpt = document.getElementById('ctl00_ContentMain_RegisterUser_rblAccept_0');
            if (accpt.checked == true)
                isChecked = true;
        }
        
        if (list != null) {
            for (var i = 0; i < list.rows.length; i++) {
                for (var j = 0; j < list.rows[i].cells.length; j++) {
                    var listControl = list.rows[i].cells[j].childNodes[0];
                    if (listControl!=null && listControl.checked) {
                        if (vid == "CustomValidator1" || "CustomValidator4")
                            isChecked = true;
                        else if(vid == "CustomValidator2" && typelist.rows[0].cells[0].childNodes[0].checked)
                            isChecked = true;
                        else if (vid == "CustomValidator3" && typelist.rows[0].cells[1].childNodes[0].checked)
                            isChecked = true;
                        else if (vid == "CustomValidator5" && typelist.rows[0].cells[0].childNodes[0].checked)
                            isChecked = true;
                    }
                }
            }
        }

        if (vid == "CustomValidator2" && !typelist.rows[0].cells[0].childNodes[0].checked)
            isChecked = true;
        else if (vid == "CustomValidator3" && !typelist.rows[0].cells[1].childNodes[0].checked)
            isChecked = true;

        return isChecked;
    }


    </script>
     <style type="text/css">
    .failureNotification
    { 
      color:Red;
    }
    
    </style>
</asp:Content>



<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentMain">


    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>



    <%--Create a new user--%>
    <asp:CreateUserWizard ID="RegisterUser" runat="server" EnableViewState="true" 
        OnNextButtonClick="SaveData"  OnCreatedUser="RegisterUser_CreatedUser">
        <LayoutTemplate>
            <asp:PlaceHolder ID="wizardStepPlaceholder" runat="server"></asp:PlaceHolder>
            <asp:PlaceHolder ID="navigationPlaceholder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
        <WizardSteps>
        
<%--BEGIN PAGE 1 REGISTRATION --%>
        
        <asp:WizardStep ID="EnrollmentStep" runat="server" >
        
        
     <div class="remsCOP-join">
    <div class="rcontainer">
    <div class="container ad">
 <div class="grid_12 omega">         
        
        
<section>
        <h2>Join Community Form</h2>
    <p>Thank you for joining the REMS TA Center Community of Practice (CoP), where you can collaborate on special projects, share news and resources, discuss trends and ideas, and learn from the experiences of others in the field. If you have created a profile for the REMS TA Center Online Trainings, please note that you will need to create a SEPARATE account to access the Community of Practice.</p>
</section>
<section>
    <p class="question">We want to know more about you and your work. Please confirm which institution type you represent.</p>
 
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        <asp:CheckBoxList ID="cblInstType" runat="server" OnSelectedIndexChanged="SetRoles" AutoPostBack="true">
            <asp:ListItem Value="K-12"> <label>K-12</label></asp:ListItem>
            <asp:ListItem Value="Higher ed"><strong>Higher Ed</strong></asp:ListItem>
            <asp:ListItem Value="Government Agency or Community Partner"><strong>Government Agency or Community Partner</strong></asp:ListItem>
        </asp:CheckBoxList>
        
    <div class="grid_12">
         <asp:CustomValidator ID="CustomValidator1" ClientValidationFunction="ValidateChkList"
            runat="server" >An institution type is required.</asp:CustomValidator>
    </div>
   

    
    <p class="question">Which of the following best describes your role?</p>
 
    <div class="grid_12 omega">
        <div class="questionBlock">
            <h5>K-12</h5>
            <asp:RadioButtonList ID="rblK12Role" runat="server" Enabled="false">
                <asp:ListItem Value="1">K-12 Teacher or Administrator</asp:ListItem>
                <asp:ListItem Value="2">Law Enforcement Officer or School Cop</asp:ListItem>
                <asp:ListItem Value="3">School Resource Officer</asp:ListItem>
                <asp:ListItem Value="4">EMS Practitioner</asp:ListItem>
                <asp:ListItem Value="5">Community Member</asp:ListItem>
                <asp:ListItem Value="6">Afterschool Services Provider</asp:ListItem>
                <asp:ListItem Value="7">Facilities Manager</asp:ListItem>
                <asp:ListItem Value="8">Parent</asp:ListItem>
            </asp:RadioButtonList>
            <asp:CustomValidator ID="CustomValidator2" ClientValidationFunction="ValidateChkList" 
            runat="server" >A K-12 role is required.</asp:CustomValidator>

        </div>
         <div class="questionBlock">
             <h5>Higher Ed</h5>
             <asp:RadioButtonList ID="rblHigherRole" runat="server"  Enabled="false">
                 <asp:ListItem Value="1">IHE Faculty, Staff, or Administrator</asp:ListItem>
                 <asp:ListItem Value="2">Law Enforcement Officer or Campus Cop</asp:ListItem>
                 <asp:ListItem Value="3">EMS Practitioner</asp:ListItem>
                 <asp:ListItem Value="4">Community Member</asp:ListItem>
                 <asp:ListItem Value="5">Campus or Building Director</asp:ListItem>
                 <asp:ListItem Value ="6">Facilities and Operations Director</asp:ListItem>
                 <asp:ListItem Value="7">Parent</asp:ListItem>
             </asp:RadioButtonList>
              <asp:CustomValidator ID="CustomValidator3" ClientValidationFunction="ValidateChkList" 
            runat="server" >A higher ed role is required.</asp:CustomValidator>
         </div>
    </div>
    </ContentTemplate>
        </asp:UpdatePanel>
        
</section>        
        
  <section>      
    <p class="question">Don’t see your role listed? Type it in the field below. </p>
    <div class="grid_12 omega">
        <asp:TextBox ID="txtOtherRoleDes" TextMode="MultiLine" runat="server"></asp:TextBox>
    </div>
  </section>  
    
    
   <section> 
    <p class="question">Do you want to stay connected?</p>
    
    <div class="grid_12 omega">
       Last, First Name:
       </div>
       <div class="grid_12 omega">
        <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox><span class="reqred">*</span> 
        <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox><span class="reqred">*</span>
        
        <div class="grid_12 omega">
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtLastName"
                                     ErrorMessage="Last name is required." />
                                    
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtFirstName"
                                     ErrorMessage="First name is required." />
    </div>
    </div>
    
    
    <div class="grid_12 omega">
        State, City:
        </div>
        
      <div class="grid_12 omega">  
        <asp:DropdownList ID="ddlState" DataSourceID="dsState" runat="server" 
                           DataTextField="Location" DataValueField="StateId" Width="250">
        </asp:DropdownList> <span class="reqred">*</span> <asp:TextBox ID="txtCity" runat="server"></asp:TextBox><span class="reqred">*</span>
        
        <div class="grid_12 omega"> 
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" InitialValue="1" Display="Dynamic" ControlToValidate="ddlState"
                                     ErrorMessage="State is required." ></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtCity"
        ErrorMessage="City is required." />
        </div>
    </div>
    
    
    
    <div class="grid_12 omega"> 
        Email Address:
     </div>
     <div class="grid_12 omega"> 
        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox><span class="reqred">*</span>
        
        <div class="grid_12 omega"> 
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="txtEmail"
        ErrorMessage="Email is required." />
		</div>
    </div>
    
    
    
    <div class="grid_12 omega"> 
        School/District OR Higher ed/Campus Community Represented: 
     </div>  
     
      <div class="grid_12 omega"> 
   <asp:TextBox ID="txtSelectedTypes" runat="server" ></asp:TextBox> <span class="reqred">*</span>
   <div class="grid_12 omega"> 
    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="txtSelectedTypes"
        ErrorMessage="Selected type is required." />
     </div>   
    </div>
</section>    
    
 <section> 
   
    <p class="question">How many years of experience do you have in the field of emergency management?</p>
  <div class="grid_12 omega">    
<asp:TextBox ID="txtyrExpEmergency" MaxLength="2" runat="server"></asp:TextBox><span class="reqred">*</span>
<ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                TargetControlID="txtyrExpEmergency"></ajax:FilteredTextBoxExtender>
                                
  <div class="grid_12 omega">                                
<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtyrExpEmergency"
        ErrorMessage="Years of experience in EM are required." />

 </div>
 </div>
 </section>   
    
    
 <section>
    
<p class="question">How many years of experience do you have in the field of education?</p>

 <div class="grid_12 omega"> 
<asp:TextBox ID="txtyrExpED" MaxLength="2" runat="server"></asp:TextBox><span class="reqred">*</span>
<ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                TargetControlID="txtyrExpED">
                                
 </ajax:FilteredTextBoxExtender>
 
   <div class="grid_12 omega"> 
<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator9" ControlToValidate="txtyrExpED"
        ErrorMessage="Years of experience in ED are required." />
        </div>
</div>
</section>

<section>
    <p class="question">I am interested in: (User to select from the following options.)</p>
      <div class="grid_12 omega"> 
        <asp:CheckBoxList ID="cblInterests" runat="server"  RepeatColumns="2">
        
            <asp:ListItem>Taking a Virtual Training Course</asp:ListItem>
            <asp:ListItem>Telling a Story</asp:ListItem>
            <asp:ListItem>Sharing an Experience and/or Content</asp:ListItem>
            <asp:ListItem>Accessing Resources </asp:ListItem>
            <asp:ListItem>Exploring Lessons Learned</asp:ListItem>
            
            <asp:ListItem>Asking Questions</asp:ListItem>
            <asp:ListItem>Providing Support </asp:ListItem>
            <asp:ListItem>Getting Support</asp:ListItem>
            <asp:ListItem>Collaborating with Others in the Field</asp:ListItem>
            <asp:ListItem>Posting Resources for Discussion</asp:ListItem>
            <asp:ListItem>Becoming a Community Steward</asp:ListItem>
            
        </asp:CheckBoxList>
         <div class="grid_12 omega">  
<asp:CustomValidator ID="CustomValidator4" ClientValidationFunction="ValidateChkList"
            runat="server" >Your interested options are required.</asp:CustomValidator>
            
   </div>
    </div>
    </section>
   <section> 
    
   <%-- <p>To view and accept the REMS TA Center CoP Codes of Conduct and Terms of Use click <a href="../3_PortalRules.aspx" target="_blank">here </a> </p>--%>
    <p><span class="reqred">*</span> - Required Field</p>
</section> 

</div>
</div>
</div>
</div>

        </asp:WizardStep>
        
<%--END PAGE 1 REGISTRATION --%>

<%--BEGIN PAGE RULES--%>     
        
        <asp:WizardStep ID="RuleStep" runat="server">

    <div class="remsCOP-content">
    <div class="rcontainer">
    <div class="container ad">
 <div class="grid_12 omega"> 
    <h2>
REMS TA Center Community Rules
    </h2>
    <p>
        The REMS TA Center Community of Practice (CoP) is a virtual space for schools, school districts, institutions of higher education (IHEs), and their community partners to collaborate on special projects, share news and resources, discuss trends and ideas, and learn from the experiences of others in the field. As a forum for practitioners in the field, content on the REMS TA Center CoP might come from various sources and might be based on the experiences of CoP members and nonmembers. Statements, opinions, resources, or any other submissions posted to the CoP by a member are not necessarily endorsed by the U.S. Department of Education (ED) or the REMS TA Center. ED and the REMS TA Center do not support the selling of products, services, or goods by vendors in the CoP. 
    </p>
    <p>
       Please remember that the community will reflect and be enhanced by the diversity of those who support our nation’s schools and IHEs. At times, community members might not share the same opinion on a topic. Our Codes of Conduct and Terms of Use are designed to ensure that all community members receive respect from their fellow members while participating. Please read and accept our Codes of Conduct and Terms of Use to ensure a productive and healthy experience for all community members. 
    </p>
    <h3 class="left">CODES OF CONDUCT:</h3>
    <ul class="general">
        <li><strong>Think twice before posting confidential or sensitive information</strong>. Our goal is to ensure the privacy and security of all CoP members. However, the REMS TA Center cannot guarantee that knowledge and information exchanged will not be shared outside the CoP. Protect yourself and others, and refrain from posting sensitive information that could cause harm if viewed outside of the CoP.</li>
        <li><strong>You are accountable for messages posted to the CoP</strong>. To ensure that you (and not your school, district, IHE, or agency) are responsible for content posted, we require that all members use their real name and official affiliation.</li>
        <li><strong>A strong and diverse community requires mutual respect</strong>. The REMS TA Center encourages CoP members to participate in forum discussions and debates. We ask that the opinions and thoughts of all members be respected. Please also consider that the CoP is open to a diverse audience, and some members might not be communicating in their primary language.</li>
    </ul>
    <h3 class="left">TERMS OF USE:</h3>
    <ol class="general">
        <li>ED and the REMS TA Center do not guarantee or warrant that any information posted by individuals on these pages is correct and disclaim any liability for any loss or damage resulting from reliance on any such information. ED and the REMS TA Center may not be able to verify, do not warrant or guarantee, and assume no liability for anything posted on this website by any other person. ED and the REMS TA Center do not endorse, support, or otherwise promote any private or commercial entity or the information, products, or services contained on those Web sites that may be reached through links on our Web site. Opinions and comments that appear in the CoP belong to the individuals who expressed them. They do not belong to or represent views of ED or the REMS TA Center.</li>    
        <li>These pages may not be used for the submission of any claim, demand, informal or formal complaint, or any other form of legal and/or administrative notice or process, or for the exhaustion of any legal and/or administrative remedy.</li>  
        <li>REMS TA Center CoP members are solely responsible for ensuring that they do not act in any manner that constitutes or forms a part of a course of conduct amounting to a violation of any state, federal, international, or other applicable law. This includes, but is not limited to, posting of content in violation of the copyright on that content.</li>  
        <li>The REMS TA Center does not allow content that is abusive, vulgar, racist, sexist, slanderous, harassing, misleading, or otherwise objectionable. We also do not allow content that promotes the selling of products, goods, and services. Content of this nature will be screened and removed. </li>  
    </ol>
    <h3 class="left">
        TERMINATION:
    </h3>
    <p>
        The U.S. Department of Education and REMS TA Center may terminate or suspend your access to all or part of the REMS TA Center CoP, including but not limited to any discussion forums on its site, for any reason, including breach of the Terms of Use or Codes of Conduct. If you are unsatisfied with the services provided by the REMS TA Center, please email <a href="mailto:info@remstacenter.org">info@remstacenter.org</a> to terminate your membership.
    </p>
    <h3 class="left">PRIVACY POLICY:</h3>
    <p>
The U.S. Department of Education and REMS TA Center will not sell any personal information about REMS TA Center CoP members to any third parties. The U.S. Department of Education and REMS TA Center may use account information generically in order to revise its services or present findings from the use of its services. 
</p>
    <asp:RadioButtonList ID="rblAccept" runat="server">
        <asp:ListItem Value="1">I accept the Terms of Use and Codes of Conduct.</asp:ListItem>
        <asp:ListItem Selected ="True" Value="0">I do not accept the Terms of Use and Codes of Conduct.</asp:ListItem>
    </asp:RadioButtonList>
    <asp:CustomValidator ID="CustomValidator5" ClientValidationFunction="ValidateChkList"
            runat="server" >You must accept our terms and conditions.</asp:CustomValidator>
            
</div>
</div>
</div>
</div>            
            
        </asp:WizardStep>

<%--END PAGE RULES--%>
<%----%>

<%--BEGIN CREATE A NEW ACCOUNT --%>

        <asp:CreateUserWizardStep ID="RegisterUserWizardStep" runat="server">
                <ContentTemplate>
                
     <div class="remsCOP-join">
    <div class="rcontainer">
    <div class="container ad">
 <div class="grid_12 omega">                  
                
                
                    <h2>Create a New Account</h2>
                    <p>Use the form below to create a new account.</p>
                    <p>Passwords are required to be a minimum of <%= Membership.MinRequiredPasswordLength %> characters in length.</p>
                    <div class="grid_12 omega">
                    <span class="failureNotification">
                        <asp:Literal ID="ErrorMessage" runat="server"></asp:Literal>
                    </span>
                    </div>
                    <div class="grid_12 omega">
                    <asp:ValidationSummary ID="RegisterUserValidationSummary" runat="server" CssClass="failureNotification" 
                         ValidationGroup="RegisterUserValidationGroup"/>
                      </div>   
                         
                    <div class="accountInfo grid_12 omega">
                        <fieldset class="register">
                            <legend>Account Information</legend>
                            
                            
                            <div class="grid_12 omega pad-top">
                         
                            <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName" CssClass="createNew">User Name:</asp:Label>
                            <asp:TextBox ID="UserName" runat="server" CssClass="textEntry"></asp:TextBox>
                           
                            
                            
                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" 
                                     CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required." 
                                     ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                            
                            </div>
                            
                            
                            <div class="grid_12 omega pad-top">
                            <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email" CssClass="createNew">E-mail:</asp:Label>
                            <asp:TextBox ID="Email" runat="server" CssClass="textEntry" Enabled="false"></asp:TextBox>
                           
                             
                            <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email" 
                                     CssClass="failureNotification" ErrorMessage="E-mail is required." ToolTip="E-mail is required." 
                                     ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                            
                            </div>
                            
                            
                             <div class="grid_12 omega pad-top">
                            <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password" CssClass="createNew">Password:</asp:Label>
                            <asp:TextBox ID="Password" runat="server" CssClass="passwordEntry" TextMode="Password"></asp:TextBox>
                            
                             
                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" 
                                     CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required." 
                                     ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                            
                            </div>
                            
                            
                            <div class="grid_12 omega pad-top">
                            <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword" CssClass="createNew">Confirm Password:</asp:Label>
                            <asp:TextBox ID="ConfirmPassword" runat="server" CssClass="passwordEntry" TextMode="Password"></asp:TextBox>
                            
                             
                            <asp:RequiredFieldValidator ControlToValidate="ConfirmPassword" CssClass="failureNotification" Display="Dynamic" 
                                     ErrorMessage="Confirm Password is required." ID="ConfirmPasswordRequired" runat="server" 
                                     ToolTip="Confirm Password is required." ValidationGroup="RegisterUserValidationGroup">*</asp:RequiredFieldValidator>
                               
                                
                                <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" 
                                     CssClass="failureNotification" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match."
                                     ValidationGroup="RegisterUserValidationGroup">*</asp:CompareValidator>
                                   
                                </div>     
                           
                        </fieldset>
                        <div class="grid_12 omega pad-top">
                            <asp:Button ID="CreateUserButton" runat="server" CommandName="MoveNext" Text="Create User" 
                                 ValidationGroup="RegisterUserValidationGroup"/>
                        </div>
                    </div>
</div>
</div>
</div>
</div>
    
                </ContentTemplate>
                <CustomNavigationTemplate>
                </CustomNavigationTemplate>
            </asp:CreateUserWizardStep>
            
 <%--END CREATE NEW ACCOUNT --%>           
            
  <%--BEGIN SUCCESS PAGE --%>          
            
             <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server">
              <ContentTemplate>
     <div class="remsCOP-content">
    <div class="rcontainer">
    <div class="container ad">
 <div class="grid_12 omega">                
              
                    <h2>Complete</h2>

                    <p class="center">Your account has been successfully created.</p>

                    <asp:Label ID="SubscribeLabel" runat="server" Text="Click Continue button to go to log in page."></asp:Label><br />

                 <div class="grid_12 omega"> 
                    <asp:Button ID="ContinueButton" OnClick="ContinueButton_Click" runat="server"  CausesValidation="False" CommandName="Continue"
                        Text="Continue" ValidationGroup="CreateUserWizard1" CssClass="btn btn-lg" />

        </div>
</div>
</div>
</div>
</div>        
        
    </ContentTemplate>
                </asp:CompleteWizardStep>
                
 <%--END SUCCESS PAGE--%>               
                
                
                
        </WizardSteps>
    </asp:CreateUserWizard>
     <div class="remsCOP-content">
    <div class="rcontainer">
    <div class="container ad">
 <div class="grid_12 omega">   
  <asp:SqlDataSource ID="dsState" runat="server" 
        ConnectionString="<%$ ConnectionStrings:AspNetForumConnectionString %>" 
        SelectCommand="SELECT [StateId], [Location] FROM [States]"></asp:SqlDataSource>
</div>
</div>
</div>
</div> 
</asp:Content>
