﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/remsForumMasterPage.master" AutoEventWireup="true"
    CodeFile="ChangePasswordSuccess.aspx.cs" Inherits="Account_ChangePasswordSuccess" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentMain">
    <h2>
        Change Password
    </h2>
    <p>
        Your password has been changed successfully.
    </p>
</asp:Content>
