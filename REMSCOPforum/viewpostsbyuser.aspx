<%@ Page Title="View User Profile" language="c#" Codebehind="viewpostsbyuser.aspx.cs" AutoEventWireup="True" Inherits="aspnetforum.viewpostsbyuser" MasterPageFile="aspnetforummaster.master" %>
<asp:Content ContentPlaceHolderID="AspNetForumContentPlaceHolder" ID="AspNetForumContent" runat="server">
    <div class="location">
	<h2><asp:Label id="lblUser" runat="server" Font-Bold="True" meta:resourcekey="lblUserResource1"></asp:Label></h2>
    
    <h2><asp:Label ID="lblAllMsgs" runat="server" EnableViewState="False" meta:resourcekey="lblAllMsgsResource1">all messages by user</asp:Label></h2></div>
	    <asp:repeater id="rptMessagesList" runat="server" EnableViewState="False">
	        <HeaderTemplate>
	            <table>
	        </HeaderTemplate>
		    <ItemTemplate>
			    <tr>
				    <td>
				        <span><%# ToAgoString((DateTime)Eval("CreationDate"))%><br />Topic:</span>
                        
				        <a href='<%# aspnetforum.Utils.Various.GetTopicURL(Eval("TopicID"), Eval("Subject")) %>'><strong><%# Eval("Subject") %></strong></a>
				        <p>
				        <%# aspnetforum.Utils.User.DisplayUserInfo(userID, userName, null, avatarFileName, firstName, lastName)%>
                        </p>
			        </td>
				    <td><%# aspnetforum.Utils.Formatting.FormatMessageHTML(Eval("Body").ToString())%></td>
			    </tr>
		    </ItemTemplate>
		    <FooterTemplate>
                </table>
				<div class="pager"><%#pagerString %></div>
		    </FooterTemplate>
	    </asp:repeater>
</asp:Content>