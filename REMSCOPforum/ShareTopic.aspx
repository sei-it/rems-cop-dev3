﻿<%@ Page Title="" Language="C#" MasterPageFile="~/REMSCOPforum/AspNetForumMaster.Master" AutoEventWireup="true" CodeFile="ShareTopic.aspx.cs" Inherits="REMSCOPforum_ShareTopic" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit.HTMLEditor" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHEAD" Runat="Server">
    <script type = "text/javascript">

    function SetTarget() {

        document.forms[0].target = "_blank";

    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AspNetForumContentPlaceHolder" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

 <div style="padding-top:90px">
<table>
<tr>
<td colspan="2" noWrap>
<asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Following error occurs:" ShowMessageBox="false" DisplayMode="BulletList" ShowSummary="true" />  
</td>
</tr>
<tr>
<td style="width: 180px; white-space: nowrap;"><asp:Label ID="UserNameLabel" runat="server" Text="Your name:" AssociatedControlID="txtName" /></td>
<td><asp:TextBox ID="txtName" runat="server" Width="500"></asp:TextBox>
<asp:RequiredFieldValidator  
             ID="RequiredFieldValidator1"  
             runat="server"  
             ControlToValidate="txtName"  
             ErrorMessage='Input your name!'  
             EnableClientScript="true"  
             SetFocusOnError="true"  
             Text="*"  
             />  
</td>

</tr>
<tr>
<td><asp:Label ID="Label1" runat="server" Text="Send to:" AssociatedControlID="txtEmailto" />
    </td>
<td><asp:TextBox ID="txtEmailto" runat="server" Width="500" ></asp:TextBox>
<asp:RequiredFieldValidator  
             ID="RequiredFieldValidator2"  
             runat="server"  
             ControlToValidate="txtEmailto"  
             ErrorMessage='Input send to!'  
             EnableClientScript="true"  
             SetFocusOnError="true"  
             Text="*"  
             /> 
<asp:RegularExpressionValidator ID="RegularExpressionValidator2"
  runat="server" ErrorMessage="Email format is wrong!" 
  ControlToValidate="txtEmailto" 
  ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*"
   EnableClientScript="true"  
   SetFocusOnError="true"  
   Text="*"></asp:RegularExpressionValidator>
</td>
</tr>
<tr>
<td><asp:Label ID="Label2" runat="server" Text="CC:&lt;br&gt;(separated by semicolon if more than one)" AssociatedControlID="txtCC" /></td>
<td><asp:TextBox ID="txtCC" runat="server" Width="500" TextMode="MultiLine"></asp:TextBox>
  <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
  runat="server" ErrorMessage="Email format is wrong!" 
  ControlToValidate="txtCC"
  ValidationExpression="^(([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$"
   EnableClientScript="true"  
   SetFocusOnError="true"  
   Text="*"></asp:RegularExpressionValidator>

</td>
</tr>
<tr>

<td><asp:Label ID="Label3" runat="server" Text="Subject:" AssociatedControlID="txtSubject" /></td>
<td><asp:TextBox ID="txtSubject" runat="server" Width="500"></asp:TextBox>
<asp:RequiredFieldValidator  
             ID="RequiredFieldValidator3"  
             runat="server"  
             ControlToValidate="txtSubject"  
             ErrorMessage='Input subject!'  
             EnableClientScript="true"  
             SetFocusOnError="true"  
             Text="*"  
             />  
</td>
</tr>
<tr>
<td style="vertical-align:top;"><asp:Label ID="Label4" runat="server" Text="Text message:" AssociatedControlID="edtBody" /></td>
<td>
    <cc1:Editor ID="edtBody" runat="server" Width="500" Height="350" />
    </td>
</tr>
</table>
<br /><br />
<div>
<asp:Button ID="btnSend" runat="server" Text="Send" onclick="btnSend_Click" />
<asp:Button ID="btnCanel" runat="server" Text="Cancel" CausesValidation="false" />
</div>
</div>
</asp:Content>



