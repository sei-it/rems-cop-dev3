<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="adminusergroups.aspx.cs" Inherits="aspnetforum.adminusergroups" MasterPageFile="AspNetForumMaster.Master" %>
<%@ Register TagPrefix="cc" Namespace="aspnetforum" Assembly="aspnetforum" %>

<asp:Content ContentPlaceHolderID="AspNetForumContentPlaceHolder" ID="AspNetForumContent" runat="server">

<p><b><asp:Label ID="lblAvailableGroups" runat="server" EnableViewState="False" meta:resourcekey="lblAvailableGroupsResource1" Text="Available user groups:"></asp:Label></b>
	<br />
	<cc:THDataGrid id="gridGroups" Runat="server"  AutoGenerateColumns="False" EnableViewState="False"
		ShowHeader="False" OnItemCommand="gridGroups_ItemCommand"  meta:resourcekey="gridGroupsResource1"
		 GridLines="None" >
		<AlternatingItemStyle CssClass="altItem" />
		<Columns>
			<asp:BoundColumn Visible="False" DataField="GroupID"></asp:BoundColumn>
			<asp:HyperLinkColumn DataNavigateUrlField="GroupID" DataTextField="Title" DataNavigateUrlFormatString="editusergroup.aspx?GroupID={0}" ></asp:HyperLinkColumn>
			<asp:TemplateColumn>
				<ItemTemplate>
                <asp:LinkButton Text="<span class=&quot;fa  fa-trash fa-large&quot; alt=&quot;&quot;></span>" runat="server" CommandName="delete" OnClientClick="return confirm('Are you sure?');"></asp:LinkButton>
                </ItemTemplate>
			</asp:TemplateColumn>
		</Columns>
	</cc:THDataGrid>
	<asp:Label ID="lblNoGroups" Runat="server" Visible="False" meta:resourcekey="lblNoGroupsResource1" Text="No groups found."></asp:Label>
</p>

<table>
	<thead>
	<tr>
    <th colspan="2"><b><asp:Label ID="lblAddNew" runat="server" EnableViewState="False" meta:resourcekey="lblAddNewResource1" Text="Add a new group:"></asp:Label></b></th>
    </tr>
    </thead>
    
	<tbody>
	<tr>
		<td><asp:Label ID="lblTitle" runat="server" EnableViewState="False" meta:resourcekey="lblTitleResource1" Text="Title:"></asp:Label></td>
		<td><asp:TextBox id="tbGroupTitle" runat="server" Width="100%" meta:resourcekey="tbGroupTitleResource1"></asp:TextBox></td>
	</tr>
	<tr>
    <td colspan="2"><asp:Button id="btnAddGroup" runat="server" Text="add" onclick="btnAddGroup_Click" meta:resourcekey="btnAddGroupResource1"></asp:Button></td>
    </tr>
	</tbody>
</table>
</asp:Content>