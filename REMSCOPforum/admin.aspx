<%@ Page language="c#" ValidateRequest="false" Codebehind="admin.aspx.cs" AutoEventWireup="True" Inherits="aspnetforum.admin" MasterPageFile="AspNetForumMaster.Master" %>
<%@ Register TagPrefix="cc" Namespace="aspnetforum" Assembly="aspnetforum" %>
<%@ Import Namespace="aspnetforum.Resources" %>

<asp:Content ContentPlaceHolderID="AspNetForumContentPlaceHolder" ID="AspNetForumContent" runat="server">

<p><a href="http://www.jitbit.com/asp-net-forum/versionhistory/" style="color:Red" id="lnkUpgrade" runat="server" visible="false" target="_blank" enableviewstate="false">A newer version detected. Click here to learn more.</a></p>


	<cc:THDataGrid id="gridForums" runat="server" AutoGenerateColumns="False" EnableViewState="False"
		ShowHeader="true" UseAccessibleHeader="true" OnItemCommand="gridForums_ItemCommand" OnItemDataBound="gridForums_ItemDataBound" GridLines="None">
		<AlternatingItemStyle CssClass="altItem" />
		<Columns>
			<asp:BoundColumn Visible="False" DataField="ForumID" HeaderText="ForumID"></asp:BoundColumn>
			<asp:HyperLinkColumn DataTextField="Title" DataNavigateUrlField="ForumID" DataNavigateUrlFormatString="editforum.aspx?ForumID={0}" HeaderText="Forums" ItemStyle-Width="40%"></asp:HyperLinkColumn>
			<asp:ButtonColumn Text="<div class='center'><span class='fa fa-arrow-circle-up fa-large'></span></div>" CommandName="up" ItemStyle-Width="5%"></asp:ButtonColumn>
			<asp:ButtonColumn Text="<div class='center'><span class='fa fa-arrow-circle-down fa-large'></span></div>" CommandName="down" ItemStyle-Width="5%"></asp:ButtonColumn>
			<asp:TemplateColumn>
            	
				<ItemTemplate><div class="center"><asp:Button runat="server" CommandName="delete" Text="<%# various.Delete %>"/></asp:Button></div></ItemTemplate>
               
               
			</asp:TemplateColumn>
		</Columns>
	</cc:THDataGrid>
	<asp:Label ID="lblNoForums" Runat="server" Visible="False"><%= various.NoForumsFound %></asp:Label>



	<table>
    	<thead>
		<tr><th colspan="2"><%= various.AddNewForum %></th></tr>
        </thead>
		<tbody>
		<tr>
			<td><%= various.Name %>:</asp:Label></td>
			<td><asp:TextBox id="tbTitle" runat="server"  MaxLength="50"></asp:TextBox></td>
		</tr>
        
		<tr>
			<td><%= various.Description %>:</asp:Label></td>
			<td><asp:TextBox id="tbDescr" runat="server"  MaxLength="255"></asp:TextBox></td>
		</tr>
        
		<tr>
			<td><%= various.ForumCategory %></td>
			<td>
				<span id="lblSelectGroup" runat="server" enableviewstate="false"><%= various.SelectCategory %></span>
				<asp:DropDownList id="ddlForumGroup" runat="server" DataTextField="GroupName" DataValueField="GroupID"></asp:DropDownList>
				<a href="forumgroups.aspx" title="edit available forum groups" runat="server" id="lnkEditForumGroups" enableviewstate="false">.?.</a>&nbsp;&nbsp;&nbsp;
				<span id="lblEnterGroup" runat="server" enableviewstate="false"><%= various.OrEnterNew %></span>
				<asp:TextBox id="tbForumGroup" runat="server"></asp:TextBox>
			</td>
		</tr>
        
		<tr>
        <td colspan="2"><asp:Button id="btnAdd" runat="server" Text="<%# various.Add %>" onclick="btnAdd_Click"></asp:Button></td>
        </tr>
        
		</tbody>
	</table>

<asp:Label ID="lblError" runat="server" Visible="False" EnableViewState="False" ForeColor="Red">Error: a forum was not created. Please fill all the fields.</asp:Label>

</asp:Content>