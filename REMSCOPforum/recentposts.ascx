﻿<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="false" CodeBehind="recentposts.ascx.cs" Inherits="aspnetforum.recentposts" %>

<asp:repeater id="rptMessagesList" runat="server" EnableViewState="False" OnItemDataBound="rptMessagesList_ItemDataBound">
	<HeaderTemplate>
		<table>
        <thead>
			<tr>
				<th colspan="2"><h2><%= aspnetforum.Resources.various.RecentPosts %>&nbsp;<a href="recent.aspx?rss=1" runat="server" id="rssLink" enableviewstate="false"><span class="fa fa-rss"></span></a></h2></th>
			</tr>
          </thead>  
			<tbody>
	</HeaderTemplate>
    
	<ItemTemplate>
		<tr <%# Container.ItemType == ListItemType.AlternatingItem ? " class='altItem'" : "" %>>
			<td>
				<span class="pad-top"><%# aspnetforum.ForumPage.ToAgoString((DateTime)Eval("CreationDate"))%><br />
				<b>Topic:</b></span>
                
				<a class="pad-top" href='<%# aspnetforum.Utils.Various.GetTopicURL(Eval("TopicID"), Eval("Subject"), true) %>#post<%# Eval("MessageID") %>'><b><%# Eval("Subject") %></b></a>
                
				<div>
					<%# aspnetforum.Utils.User.DisplayUserInfo(Eval("UserID"), Eval("UserName"), Eval("PostsCount"), Eval("AvatarFileName"), Eval("FirstName"), Eval("LastName"))%>
                </div>
			</td>
            
			<td>
				<span class="pad-top"><%# aspnetforum.Utils.Formatting.FormatMessageHTML(Eval("Body").ToString())%></span>
			</td>
		</tr>
		<tr class="utils <%# Container.ItemType == ListItemType.AlternatingItem ? " altItem" : "" %>">
			<td></td>
			<td><a runat="server" id="lnkQuote" visible="False"><%= aspnetforum.Resources.various.ReplyWithQuote %></a></td>
		</tr>
	</ItemTemplate>
	<FooterTemplate>
    </tbody>
    </table>
    </FooterTemplate>
</asp:repeater>