<%@ Page Language="C#" AutoEventWireup="true" Title="Recently updated topics" CodeBehind="recenttopics.aspx.cs" Inherits="aspnetforum.recenttopics" MasterPageFile="AspNetForumMaster.Master" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolderHEAD" ID="AspNetForumHead" runat="server">
<link rel="alternate" type="application/rss+xml" title="recent posts" href="recenttopics.aspx?rss=1" />
</asp:Content>

<asp:Content ContentPlaceHolderID="AspNetForumContentPlaceHolder" ID="AspNetForumContent" runat="server">
	<div class="location">
		<h2><a href="default.aspx"><%= aspnetforum.Resources.various.Home %></a></h2>
	</div>
	<asp:repeater id="rptTopicsList" runat="server" EnableViewState="False">
		<HeaderTemplate>
			<table id="recentTopics">
            <thead>
			<tr>
				<th><h2><%= aspnetforum.Resources.various.RecentTopics %> <a href="recenttopics.aspx?rss=1" runat="server" id="rssLink" enableviewstate="false"><span class="fa fa-rss"></span></a></h2></th>
				<th><%= aspnetforum.Resources.various.LatestPost %></th>
			</tr>
           </thead> 
			<tbody>
		</HeaderTemplate>
		<ItemTemplate>
			<tr <%# Container.ItemType == ListItemType.AlternatingItem ? " class='altItem'" : "" %>>
				<td><h2><a href='<%# aspnetforum.Utils.Various.GetTopicURL(Eval("TopicID"), Eval("Subject")) %>'><%# Eval("Subject") %></a></h2>	
				</td>
				<td><%# aspnetforum.Utils.Message.FormatMessageInfo(Eval("LastMessageID"), Eval("TopicID"), Eval("LastUserID"), Eval("LastUserName"), Eval("LastFirstName"), Eval("LastLastName"), Eval("Subject"), Eval("CreationDate"), null, Eval("Body"))%></td>
				</td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</tbody>
            
            </table>
		</FooterTemplate>
	</asp:repeater>
	
	<div class="location">
		<h2><a href="default.aspx"><%= aspnetforum.Resources.various.Home %></a>&raquo;
		<span><%= aspnetforum.Resources.various.RecentTopics%></span></h2>
	</div>
</asp:Content>