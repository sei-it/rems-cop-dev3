<%@ Page Language="C#" MasterPageFile="AspNetForumMaster.Master" AutoEventWireup="true" CodeBehind="forumgroups.aspx.cs" Inherits="aspnetforum.forumgroups" Title="Untitled Page" %>
<%@ Register TagPrefix="cc" Namespace="aspnetforum" Assembly="aspnetforum" %>

<asp:Content ID="Content2" ContentPlaceHolderID="AspNetForumContentPlaceHolder" runat="server">
	
	<asp:Label ID="lblEditForumGroups" runat="server" EnableViewState="False">Editing forum categories</asp:Label>
	
	<cc:THDataGrid id="gridForumGroups" runat="server" AutoGenerateColumns="False"  UseAccessibleHeader="true" ShowHeader="false"
		GridLines="None"  OnEditCommand="gridForumGroups_EditCommand" OnCancelCommand="gridForumGroups_CancelCommand"
		OnUpdateCommand="gridForumGroups_UpdateCommand" OnItemCommand="gridForumGroups_ItemCommand" OnItemDataBound="gridForumGroups_ItemDataBound">
		
		<Columns>
			<asp:BoundColumn ReadOnly="true" DataField="GroupID" HeaderText="GroupID" Visible="false"></asp:BoundColumn>
			<asp:TemplateColumn HeaderText="GroupName" >
				<ItemTemplate><%# Eval("GroupName") %></ItemTemplate>
				<EditItemTemplate><asp:TextBox runat="server" Text='<%# Eval("GroupName") %>' MaxLength="50" /></EditItemTemplate>
			</asp:TemplateColumn>
			<asp:ButtonColumn Text="<span class='fa fa-arrow-circle-up icon-large'/></span>" CommandName="up"></asp:ButtonColumn>
			<asp:ButtonColumn Text="<span class='fa fa-arrow-circle-down icon-large'/></span>" CommandName="down"></asp:ButtonColumn>
			<asp:EditCommandColumn CancelText="<span>Cancel</span>" EditText="<span>Edit</span>" UpdateText="<span>Update</span>"></asp:EditCommandColumn>
			<asp:ButtonColumn Text="<span class='fa fa-trash icon-large'/></span>" CommandName="delete"></asp:ButtonColumn>
		</Columns>
	</cc:THDataGrid>
	<div>
	<asp:TextBox id="tbForumGroup" runat="server"></asp:TextBox>
	<asp:Button runat="server" ID="btnAdd" Text="+" onclick="btnAdd_Click" />
    </div>
</asp:Content>
