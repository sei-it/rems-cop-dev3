﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MKB.TimePicker;


public partial class admin_managecalendar : System.Web.UI.Page
{
    remsUsersDataClassesDataContext db = new remsUsersDataClassesDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadData(0);
        }
    }
    protected void OnNew(object sender, EventArgs e)
    {
        hfID.Value = "";
        TextBox1.Text = "";
        TextBox2.Text = "";
        TextBox3.Text = "";
        TextBox4.Text = "";
        ddlTimeZone.SelectedIndex = 0;
        mpeNewsWindow.Show();
    }
    protected void OnDelete(object sender, EventArgs e)
    {
        string id = (sender as LinkButton).CommandArgument;
        //db.Calendars.Delete(x => x.ID == Convert.ToInt32(id));
        Calendar calendar1 = db.Calendars.SingleOrDefault(x => x.ID == Convert.ToInt32(id));
        db.Calendars.DeleteOnSubmit(calendar1);
        db.SubmitChanges();
        LoadData(0);
    }
    protected void OnEdit(object sender, EventArgs e)
    {
        string id = (sender as LinkButton).CommandArgument;
        hfID.Value = id;
        Calendar data = db.Calendars.SingleOrDefault(x => x.ID == Convert.ToInt32(id));
        TextBox1.Text = data.CalendarTitle;
        TextBox2.Text = data.CalendarDescription;
        TextBox3.Text = ((DateTime)data.CalendarStart).ToShortDateString();
        TextBox4.Text = ((DateTime)data.CalendarEnd).ToShortDateString();
        int hour = ((DateTime)data.CalendarStart).Hour;
        TimeSelector.AmPmSpec ampm = TimeSelector.AmPmSpec.AM;
        if (hour >= 12)
        {
            hour -= 12;
            ampm = TimeSelector.AmPmSpec.PM;
        }
        TimeSelector1.SetTime(hour, ((DateTime)data.CalendarStart).Minute, ampm);
        hour = ((DateTime)data.CalendarEnd).Hour;
        ampm = TimeSelector.AmPmSpec.AM;
        if (hour >= 12)
        {
            hour -= 12;
            ampm = TimeSelector.AmPmSpec.PM;
        }
        TimeSelector2.SetTime(hour, ((DateTime)data.CalendarEnd).Minute, ampm);
        if (!string.IsNullOrEmpty(data.TimeZone))
            ddlTimeZone.SelectedValue = data.TimeZone;
        LoadData(0);
        mpeNewsWindow.Show();
    }
    protected void OnGridViewPageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        LoadData(e.NewPageIndex);
    }
    protected void OnAllDayEvent(object sender, EventArgs e)
    {
        if (CheckBox1.Checked)
        {
            TimeSelector1.Enabled = false;
            TimeSelector2.Enabled = false;
        }
        else
        {
            TimeSelector1.Enabled = true;
            TimeSelector2.Enabled = true;
        }
        mpeNewsWindow.Show();
    }
    protected void OnSave(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(TextBox1.Text) && !string.IsNullOrEmpty(TextBox3.Text) && !string.IsNullOrEmpty(TextBox4.Text))
        {
            Calendar data = new Calendar();
            if (!string.IsNullOrEmpty(hfID.Value))
            {
                data = db.Calendars.SingleOrDefault(x => x.ID == Convert.ToInt32(hfID.Value));
            }
            else
            {
                data.ContentGroupID = 1;
            }

            if (CheckBox1.Checked)
            {
                data.CalendarStart = DateTime.Parse(TextBox3.Text + " 0:0:0");
                data.CalendarEnd = DateTime.Parse(TextBox4.Text + " 23:59:59");
            }
            else
            {
                data.CalendarStart = DateTime.Parse(TextBox3.Text + " " + TimeSelector1.Hour.ToString() + ":" + TimeSelector1.Minute.ToString() + ":0" + TimeSelector1.AmPm.ToString());
                data.CalendarEnd = DateTime.Parse(TextBox4.Text + " " + TimeSelector2.Hour.ToString() + ":" + TimeSelector2.Minute.ToString() + ":0" + TimeSelector2.AmPm.ToString());
            }
            data.TimeZone = ddlTimeZone.SelectedValue;
            data.WholeTitle = TextBox1.Text.Trim();
            data.CalendarTitle = (TextBox1.Text).Trim().Length>30?TextBox1.Text.Substring(0,30) +"...":TextBox1.Text;
            data.CalendarDescription = TextBox2.Text;
            if (string.IsNullOrEmpty(hfID.Value))
                db.Calendars.InsertOnSubmit(data);
            
            db.SubmitChanges();
            //data.Save();
            LoadData(0);
        }
    }
    private void LoadData(int PageNumber)
    {
        //var dataList = db.Calendars.Find(x => 1 == 1).OrderByDescending(x => x.ID).ToList();
        var dataList = from cld in db.Calendars
                       orderby cld.ID descending
                       select cld;
        GridView1.DataSource = dataList;
        GridView1.PageIndex = PageNumber;
        GridView1.DataBind();
    }
}
