﻿<%@ Page language="c#" Codebehind="privateinbox.aspx.cs" AutoEventWireup="True" Inherits="aspnetforum.privateinbox" MasterPageFile="AspNetForumMaster.Master" %>
<%@ Import Namespace="aspnetforum.Resources" %>

<asp:Content ContentPlaceHolderID="AspNetForumContentPlaceHolder" ID="AspNetForumContent" runat="server">
<div class="location">
	<h2>◀ <a href="default.aspx"><%= various.Home %></a>
	&raquo;
	<a href="editprofile.aspx"><%= various.MyProfile %></a>
	&raquo;
	<a href="privateinbox.aspx">PM</a>
	</h2></div>

<asp:Label ID="lblNotLoggedIn" runat="server" Visible="False" Font-Bold="True" ForeColor="Red">You are not signed in as a member. Please sign in to access your private messages.</asp:Label>
 <p><asp:Button ID="btnMassPM" runat="server" Text="Send mass messages" 
         onclick="btnMassPM_Click" /></p>
<asp:repeater id="rptConversationsList" runat="server" EnableViewState="False" OnItemCommand="rptConversationsList_ItemCommand">
	<HeaderTemplate>
		<table>
        <thead>
		<tr>
			<th><%= various.From %></th>
			<th><%= various.LatestPost %></th>
			<th><%= various.Posts %></th>
            <th></th>
		</tr>
        </thead>
		<tbody>
	</HeaderTemplate>
	<ItemTemplate>
		<tr <%# Container.ItemType == ListItemType.AlternatingItem ? " class='altItem'" : "" %>>
			<td>
				<%# Convert.ToInt32(Eval("New"))==2 ? "<img src='images/topic_unread.png' title='unread' alt='unread'/>" : "" %>

				<h2>
					<img alt="" class="avatar" height="32" width="32" src="<%# aspnetforum.Utils.User.GetAvatarFileName(Eval("AvatarFileName"), Eval("UseGravatar"), Eval("Email")) %>" />
					<a href='privateinbox.aspx?UserID=<%# Eval("UserID") %>'>
						<%# aspnetforum.Utils.User.GetUserDisplayName(Eval("UserName"),Eval("FirstName"), Eval("LastName"))%>
					</a>
				</h2>
			</td>
			<td class="center"><%# Eval("LastMessageDate") %></td>
			<td class="center"><%# Eval("Posts") %></td>
            <td class="center"><asp:LinkButton OnClientClick="if(!confirm('Are you sure?')) return false;" ToolTip="delete" id="btnModeratorDelete" Runat="server" CommandName="delete" CommandArgument='<%# Eval("UserID") %>'><span class="fa  fa-trash fa-large"></span></asp:LinkButton></td>
		</tr>
	</ItemTemplate>
	<FooterTemplate>
		</tbody>
        </table>
	</FooterTemplate>
</asp:repeater>

<asp:repeater id="rptMessagesList" runat="server" EnableViewState="False" 
	OnItemCommand="rptMessagesList_ItemCommand" 
	onitemdatabound="rptMessagesList_ItemDataBound">
	<HeaderTemplate>
    <table>
    </HeaderTemplate>
	<ItemTemplate>
		<tr  <%# Container.ItemType == ListItemType.AlternatingItem ? " " : "" %>>
			<td>
				<span id="span1" runat="server" class="unread fa fa-envelope-square" visible='<%# Convert.ToBoolean(Eval("New")) %>'> Unread</span>
				<span><%# ToAgoString((DateTime)Eval("CreationDate")) %></span>
				<p>
				<%# aspnetforum.Utils.User.DisplayUserInfo(Eval("UserID"), Eval("UserName"), null, Eval("AvatarFileName"), Eval("FirstName"), Eval("LastName"))%>
                </p>
			</td>
			<td>
				<%# aspnetforum.Utils.Formatting.FormatMessageHTML(Eval("Body").ToString())%>
				<%# aspnetforum.Utils.Formatting.FormatSignature(Eval("Signature").ToString())%>
				<asp:Repeater ID="rptFiles" runat="server">
				<HeaderTemplate>
					<br /><br />
					<div>
						<%# various.Attachments %><br />
				</HeaderTemplate>
				<ItemTemplate>
					<a href='getattachment.ashx?personal=1&fileid=<%# Eval("FileID") %>'>
					<%# aspnetforum.Utils.Attachments.GetThumbnail(Eval("FileName").ToString(), Convert.ToInt32(Eval("UserID"))) %>
					<%# Eval("FileName") %></a><br />
				</ItemTemplate>
				<FooterTemplate>
                </div>
                </FooterTemplate>
				</asp:Repeater>
			</td>
		</tr>
		<tr class="utils <%# Container.ItemType == ListItemType.AlternatingItem ? " altItem" : "" %>">
			<td></td>
			<td>
				<a href='addprivatemsg.aspx?ToUserID=<%# _userId %>&amp;Quote=<%# Eval("MessageID") %>'>
				<%# various.ReplyWithQuote%></a>
				&bull;
				<asp:LinkButton id="btnDelete" runat="server" OnClientClick="if(!confirm('Are you sure?')) return false;" CommandName="delete" CommandArgument='<%# Eval("MessageID") %>'><%#various.Delete %></asp:LinkButton>
			</td>
		</tr>
	</ItemTemplate>
	<FooterTemplate>
		</table>
		<div class="pager">
			<%# pagerString %>
		</div>
	</FooterTemplate>
</asp:repeater>
</asp:Content>