<%@ Page language="c#" Codebehind="viewprofile.aspx.cs" EnableViewState="false" AutoEventWireup="True" Inherits="aspnetforum.viewprofile" MasterPageFile="aspnetforummaster.master" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolderHEAD" ID="aspnetHead" runat="server">
<style>
.Text{
        font-family: Verdana, Arial, Sans-serif, 'Times New Roman';
        font-size: 8pt;
        font-weight: normal;
        font-style: normal;
        color: #333333;
        text-decoration: none;
}
.toolTip {
        font-family: Verdana, Arial, Sans-serif, 'Times New Roman';
        font-size: 8pt;
        filter:alpha(opacity=80);
        -moz-opacity: 0.8;
        opacity: 0.8;
        background-color:Yellow;
        /* comment the above 3 line if you don't want transparency*/
}

</style>
<script language="javascript" defer="false">
    //browser detection
    var agt = navigator.userAgent.toLowerCase();
    var is_major = parseInt(navigator.appVersion);
    var is_minor = parseFloat(navigator.appVersion);

    var is_nav = ((agt.indexOf('mozilla') != -1) && (agt.indexOf('spoofer') == -1)
                && (agt.indexOf('compatible') == -1) && (agt.indexOf('opera') == -1)
                && (agt.indexOf('webtv') == -1) && (agt.indexOf('hotjava') == -1));
    var is_nav4 = (is_nav && (is_major == 4));
    var is_nav6 = (is_nav && (is_major == 5));
    var is_nav6up = (is_nav && (is_major >= 5));
    var is_ie = ((agt.indexOf("msie") != -1) && (agt.indexOf("opera") == -1));
</script>
<script language="javascript" type="text/javascript">
    $(function () {
        initToolTips();
    });

//tooltip Position
var offsetX = 0;
var offsetY = 200;
var opacity = 100;
var toolTipSTYLE;

function initToolTips(){
  if(document.getElementById){
          toolTipSTYLE = document.getElementById("toolTipLayer").style;
  }
  if(is_ie || is_nav6up)
  {
    toolTipSTYLE.visibility = "visible";
    toolTipSTYLE.display = "none";
    document.onmousemove = moveToMousePos;
  }
}
function moveToMousePos(e)
{
  if(!is_ie){
    x = e.pageX;
    y = e.pageY;
  }else{
    x = event.x + document.body.scrollLeft;
    y = event.y + document.body.scrollTop;
  }

  toolTipSTYLE.left = (x/2 + offsetX)+'px';
  //  toolTipSTYLE.top = (y + offsetY)+'px';
  toolTipSTYLE.top = (y + offsetY)/2 + 'px';
  return true;
}


function toolTip(msg, fg, bg)
{
  if(toolTip.arguments.length < 1) // if no arguments are passed then hide the tootip
  {
    if(is_nav4)
        toolTipSTYLE.visibility = "hidden";
    else
        toolTipSTYLE.display = "none";
  }
  else // show
  {
    if(!fg) fg = "#777777";
    if(!bg) bg = "#ffffe5";
//    var content = '<table border="0" cellspacing="0" cellpadding="0" class="toolTip"><tr><td bgcolor="' + fg + '">' +
//                                  '<table border="0" cellspacing="1" cellpadding="0"<tr><td bgcolor="' + bg + '">'+
//                                  '<font face="sans-serif" color="' + fg + '" size="-2">' + msg +
//                                  '</font></td></tr></table>'+
//                                  '</td></tr></table>';
    var content = '<span class="toolTip" >To send a message to a community member, (1) click Users at the top, (2) select the User name, (3) send your message. </span>';

   if(is_nav4)
    {
      toolTipSTYLE.document.write(content);
      toolTipSTYLE.document.close();
      toolTipSTYLE.visibility = "visible";
    }

    else if(is_ie || is_nav6up)
    {
      document.getElementById("toolTipLayer").innerHTML = content;
      toolTipSTYLE.display='block'
    }
  }
}

s = '<table width="100%" cellspacing="2" cellpadding="0" border="0">';
s += '<tr><td><img src="http://upload.wikimedia.org/wikipedia/meta/2/2a/Nohat-logo-nowords-bgwhite-200px.jpg" border="0"/> </td><td valign="top">WikiPedia</td></tr>';
s += '<tr><td colspan="2" class="Text"><hr/>this is a test for simple tooltip. <br/>You can add text and images to the tooltip</td></tr>';
s += '</table>'

function show(){
        toolTip(s)
}

</script>
</asp:Content>
<asp:Content ContentPlaceHolderID="AspNetForumContentPlaceHolder" ID="AspNetForumContent" runat="server">
<div id="toolTipLayer" style="position:absolute; visibility: hidden;left:0;right:0"></div>
	<div class="location">
	<h2><asp:Label id="lblUser" runat="server" meta:resourcekey="lblUserResource1"></asp:Label>
		-
		<asp:Label ID="lblProfile" runat="server" meta:resourcekey="lblProfileResource1">profile</asp:Label></h2>
	</div>

	<div>
	<table>
		<tr>
			<td>
				<img id="imgAvatar" runat="server" class="avatar" src="images/guestavatar.gif" alt="User avatar" />
			</td>
			<td><asp:Label ID="lblUsernameTitle"  runat="server" meta:resourcekey="lblUsernameTitleResource1">UserName:</asp:Label></td>
			<td>
				<asp:Label id="lblUserName" runat="server"></asp:Label>
				<asp:Label id="lblFullName" runat="server"></asp:Label>
			</td>
		</tr>
		<tr>
			<td><asp:Label ID="lblTotalPosts"  runat="server" meta:resourcekey="lblTotalPostsResource1">Total posts:</asp:Label></td>
			<td colspan="2"><a id="lnkViewPosts" runat="server"></a></td>
		</tr>
		<tr id="trRating" runat="server">
			<td><asp:Label ID="lblRating"  runat="server" meta:resourcekey="lblRatingResource1">Reputation:</asp:Label></td>
			<td colspan="2"><asp:Label ID="lblRatingValue" runat="server"></asp:Label></td>
		</tr>
		<tr>
			<td><asp:Label ID="lblReggedSince"  runat="server" meta:resourcekey="lblReggedSinceResource1">Registered since:</asp:Label></td>
			<td colspan="2"><asp:Label id="lblRegistrationDate" runat="server"></asp:Label></td>
		</tr>
		<tr>
			<td><asp:Label ID="lblLastLogonDate" runat="server" meta:resourcekey="lblLastLogonDateResource1">Last logon date:</asp:Label></td>
			<td colspan="2"><asp:Label id="lblLastLogonDateValue" runat="server"></asp:Label></td>
		</tr>
		<tr>
			<td><asp:Label ID="lblInterestsTitle"  runat="server" meta:resourcekey="lblInterestsTitleResource1">Interests:</asp:Label></td>
			<td colspan="2"><asp:Label id="lblInterests" runat="server"></asp:Label></td>
		</tr>
		<tr>
			<td><asp:Label ID="lblHomepage" runat="server" meta:resourcekey="lblHomepageResource1">Homepage:</asp:Label></td>
			<td colspan="2"><asp:HyperLink id="homepage" Target="_blank" runat="server" rel="nofollow"></asp:HyperLink></td>
		</tr>
	</table>
	
	<div id="divAchievements" runat="server" visible="false">
	<table>
		<tr><th colspan="2">Achievements</th></tr>
		<asp:Repeater id="rptAchievements" runat="server" EnableViewState="False">
			<ItemTemplate>
				<tr><td><%# (bool)Eval("Achieved") == true? "<img src='images/ok.png' alt='You got it!' />":"" %></td>
				<td><b><%#Eval("Name") %></b><br />
				<%# Eval("Description") %>
				</td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
	</div>

	<br />
	<div id="divGroups" runat="server">
	<span>Member of groups:</span>
	<asp:GridView id="gridGroups" Runat="server" Width="100%" AutoGenerateColumns="False"
		ShowHeader="False" GridLines="None">
		
		<Columns>
			<asp:HyperLinkField DataTextField="Title" DataNavigateUrlFields="GroupID" DataNavigateUrlFormatString="editusergroup.aspx?GroupID={0}"></asp:HyperLinkField>
		</Columns>
	</asp:GridView>
	</div>

	</div>
    
	<div class="usertools">
    <ul>
    
		<li><asp:Button id="btnEditUser" runat="server" Text="edit..." meta:resourcekey="btnEditUserResource1"></asp:Button></li>
		<% if(aspnetforum.Utils.User.CurrentUserID!=0 && aspnetforum.Utils.Settings.EnablePrivateMessaging) { %>
			<button type="button" onmouseover="show()" onmouseout="toolTip()"
            onclick="document.location='addprivatemsg.aspx?ToUserID=<%= _userId %>'"><asp:Label ID="Label5" runat="server" meta:resourcekey="Label5Resource1">Send a private message</asp:Label></button>		
        <% } %>

		<li><asp:Button id="btnDelUser" runat="server" Text="delete this user (visible to administrators only)" onclick="btnDelUser_Click" OnClientClick="return confirm('are you sure?')" meta:resourcekey="btnDelUserResource1"></asp:Button></li>
		<li><asp:Button id="btnActivateUser" runat="server" Text="activate this user (visible to administrators only)" meta:resourcekey="btnActivateUserResource1" OnClick="btnActivateUser_Click"></asp:Button></li>
		<li><asp:Button id="btnResendActivaton" runat="server" Text="resend activation email (visible to administrators only)" meta:resourcekey="btnResendActivatonResource1" OnClick="btnResendActivaton_Click"></asp:Button></li>
		<li><asp:Button id="btnDisableUser" runat="server" Text="disable this user (visible to administrators only)" meta:resourcekey="btnDisableUserResource1" OnClick="btnDisableUser_Click"></asp:Button></li>
		<li><asp:Button id="btnMakeAdmin" runat="server" Text="grant Administrator permissions (visible to administrators only)" meta:resourcekey="btnMakeAdminResource1" OnClick="btnMakeAdmin_Click"></asp:Button></li>
		<li><asp:Button id="btnRevokeAdmin" runat="server" Text="revoke Administrator permissions (visible to administrators only)" meta:resourcekey="btnRevokeAdminResource1" OnClick="btnRevokeAdmin_Click"></asp:Button></li>
		<li><asp:Button id="btnDeleteAllPostsAndTopics" runat="server" Text="delete all posts from this user (visible to administrators only)" OnClientClick="return confirm('are you sure?')" OnClick="btnDeleteAllPostsAndTopics_Click"></asp:Button></li>
		<li><asp:Button id="btnDeleteAllPostsAndDelete" runat="server" Text="delete all posts and delete user (visible to administrators only)" OnClientClick="return confirm('are you sure?')" OnClick="btnDeleteAllPostsAndDelete_Click"></asp:Button></li>
        
      </ul>  
	</div>
</asp:Content>