﻿<%@ Page language="c#" Codebehind="topics.aspx.cs" EnableViewState="false" AutoEventWireup="True" Inherits="aspnetforum.topics" MasterPageFile="AspNetForumMaster.Master" %>
<%@ Import Namespace="aspnetforum.Resources" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolderHEAD" ID="AspNetForumHead" runat="server">
<link rel="alternate" type="application/rss+xml" title="topics in this forum" id="rssDiscoverLink" runat="server" />
</asp:Content>

<asp:Content ContentPlaceHolderID="AspNetForumContentPlaceHolder" ID="AspNetForumContent" runat="server">

<div class="location">
	<span class="smalltoolbar">
		<span id="spanRegister" runat="server" enableviewstate="false">
			<%= various.PleaseLoginRegisterToCreateTopic %>
		</span>
		<span id="spanSubscribe" runat="server" enableviewstate="false">
			<asp:LinkButton id="btnSubscribe" Runat="server" Visible="False" onclick="btnSubscribe_Click" EnableViewState="False"><%= various.WatchForTopics %></asp:LinkButton>
			<asp:LinkButton id="btnUnsubscribe" Runat="server" Visible="False" onclick="btnUnsubscribe_Click" EnableViewState="False"><%= various.StopWatchingForum %></asp:LinkButton> 
			<asp:LinkButton id="btnSubscribeMsgs" Runat="server" Visible="False" onclick="btnSubscribeMsgs_Click" EnableViewState="False"><%= various.WatchForPosts %></asp:LinkButton>
			<asp:LinkButton id="btnUnsubscribeMsgs" Runat="server" Visible="False" onclick="btnUnsubscribeMsgs_Click" EnableViewState="False"><%= various.StopWatchingForumPosts %></asp:LinkButton>
		</span>
	</span>
	<h2>◀ <a href="default.aspx"><%= various.Home %></a></h2>
    <div style="clear:both;">
	<h1>
    	<asp:Label id="lblCurForum" runat="server" EnableViewState="False"></asp:Label>
		<span id="spanAddTopic" class="postbutton" runat="server" enableviewstate="false">
		<%--<a id="newtopicID"  class="btn" href='<%# string.Format("addpost.aspx?ForumID={0}", Eval("_forumID"))%>'><%= various.NewTopic %></a>
        <asp:LinkButton CssClass="btn" ID="LinkButton1" runat="server" PostBackUrl='<%# string.Format("addpost.aspx?ForumID={0}", Eval("_forumID"))%>' ><%= various.NewTopic %></asp:LinkButton>--%>
        <a class="btn" href="addpost.aspx?ForumID=<%= _forumID %>"><%= various.NewTopic %></a>
		</span>

	</h1>
</div>
	<a runat="server" id="rssLink" enableviewstate="false"><span class="fa fa-rss icon-large"></span></a>
	<span id="divDescription" runat="server"></span>
</div>


<asp:repeater id="rptSubForumsList" runat="server" EnableViewState="False">
	<HeaderTemplate>
		<table>
        <thead>
        <tr>
        <th colspan="3"><%= various.SubForums %></th>
        </tr>
        </thead>
        
		<tbody>
	</HeaderTemplate>
	<ItemTemplate>
		<tr <%# Container.ItemType == ListItemType.AlternatingItem ? " class='altItem'" : "" %> >
			<td class="center"><img alt="" src="<%# aspnetforum.forums.GetForumIcon(Eval("IconFile")) %>" height="32" width="32" /></td>
			<td>
            	<h2><a href='<%# aspnetforum.Utils.Various.GetForumURL(Eval("ForumID"), Eval("Title")) %>'><%# Eval("Title") %></a></h2><span><%# Eval("Description") %></span>
			</td>
			<td class="center">
				<%# Eval("Topics") %></td>
			<td>
				<%# aspnetforum.Utils.Topic.GetTopicInfoBMessageyID(Eval("LatestMessageID"), Cmd)%>
            </td>
		</tr>
	</ItemTemplate>
	<FooterTemplate>
		</tbody></table>
		<br />
	</FooterTemplate>
</asp:repeater>

<asp:Label ID="lblDenied" Runat="server" ForeColor="Red" Font-Bold="True" Visible="False" EnableViewState="False">	<%= various.AccessToForumDenied %></asp:Label>
<div id="divMain" runat="server" enableviewstate="false">
	
	<table id="tblError" runat="server" visible="false" enableviewstate="false">
	<tr>
        <td>
            <div id="divError" runat="server" enableviewstate="false" visible="false">
            </div>
        </td>
    </tr>
	</table>
	
	<asp:repeater id="rptTopicsList" runat="server" EnableViewState="False" OnItemDataBound="rptTopicsList_ItemDataBound" OnItemCommand="rptTopicsList_ItemCommand">
		<HeaderTemplate>
			<table>
            	<thead>
                    <tr>
                        <th></th>
                        <th nowrap="nowrap"><%= various.LatestPost %></th>
                        <th><%= various.Views %></th>
                        <th><%= various.Posts %></th>
                    </tr>
            </thead>
            
			<tbody>
		</HeaderTemplate>
		<ItemTemplate>
			<tr <%# Container.ItemType == ListItemType.AlternatingItem ? " class='altItem'" : "" %>>
				<td>
					<img alt="topic" id="imgTopic" runat="server" enableviewstate="false"  />
					<span class="topictools">
						<asp:LinkButton id="btnModeratorApprove" Runat="server" Visible="False" CommandName="approve" CommandArgument='<%# Eval("TopicID") %>'><span class="fa fa-angle-up fa-large"></span></asp:LinkButton>
						<asp:LinkButton ID="btnModeratorStick" runat="server" Visible="False" ToolTip="stick" CommandName="stick" CommandArgument='<%# Eval("TopicID") %>'><span class="fa fa-map-marker fa-large"></span></asp:LinkButton>
						<asp:LinkButton ID="btnModeratorUnStick" runat="server" Visible="False" ToolTip="unstick" CommandName="unstick" CommandArgument='<%# Eval("TopicID") %>'><span class="fa fa-map-marker fa-large" alt="lock"></span></asp:LinkButton>
						<asp:LinkButton ID="btnModeratorClose" runat="server" Visible="False" ToolTip="close" CommandName="close" CommandArgument='<%# Eval("TopicID") %>'><span class="fa fa-lock fa-large" alt="lock"></span></asp:LinkButton>
						<asp:LinkButton ID="btnModeratorReopen" runat="server" Visible="False" CommandName="reopen" CommandArgument='<%# Eval("TopicID") %>'><span class="fa  fa-lock fa-large" alt="lock"></span></asp:LinkButton>
						<asp:LinkButton OnClientClick="if(!confirm('Are you sure?')) return false;" ToolTip="delete" id="btnModeratorDelete" Runat="server" Visible="False" CommandName="delete" CommandArgument='<%# Eval("TopicID") %>'><span class="fa  fa-trash fa-large" alt="delete"></span></asp:LinkButton>
					</span>
					<h2><a href='<%# aspnetforum.Utils.Various.GetTopicURL(Eval("TopicID"), Eval("Subject")) %>'> <%# Eval("Subject") %></a></h2>
					<span><%# ShowPageLinks(Eval("Messages"), Eval("TopicID"), Eval("Subject"))%></span>
					<span><%= various.From %>&nbsp;<%# DisplayUserName(Eval("UserName"), Eval("UserID"), Eval("FirstName"), Eval("LastName"))%></span>
				</td>
				<td class="center">
					<%# aspnetforum.Utils.Message.FormatMessageInfo(Eval("LastMessageID"), Eval("TopicID"), Eval("LastUserID"), Eval("LastUserName"), Eval("LastFirstName"), Eval("LastLastName"), Eval("Subject"), Eval("CreationDate"), Eval("Messages") as int?, Eval("Body"))%>
                    
                 </td>
				<td class="center">
					<%# Eval("ViewsCount") %>
                </td>
				<td class="center" >
					<%# Eval("Messages") %>
                </td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
			</tbody>
            </table>
		</FooterTemplate>
	</asp:repeater>

	<div class="pager"><%= pagerString %></div>
	
</div>
        </span>
    </div>
</asp:Content>