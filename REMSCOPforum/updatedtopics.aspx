﻿<%@ Page Title="" Language="C#" MasterPageFile="AspNetForumMaster.Master" AutoEventWireup="true" CodeBehind="updatedtopics.aspx.cs" Inherits="aspnetforum.updatedtopics" %>
<asp:Content ContentPlaceHolderID="AspNetForumContentPlaceHolder" runat="server">

<div class="location">
	<h2><a href="default.aspx"><%= aspnetforum.Resources.various.Home %></a></h2>

	<h2><%= aspnetforum.Resources.various.UnreadTopics %></h2>
</div>


<asp:repeater id="rptTopicsList" runat="server" EnableViewState="False">
	<HeaderTemplate>
		<table>
        <thead>
		<tr>
        <th></th>
        <th><%= aspnetforum.Resources.various.LatestPost %></th>
        </tr>
        </thead>
		<tbody>
	</HeaderTemplate>
	<ItemTemplate>
		<tr>
			<td width="70%"><h2><a href='<%# aspnetforum.Utils.Various.GetTopicURL(Eval("TopicID"), Eval("Subject")) %>'>
						<%# Eval("Subject") %>
					</a>
				</h2>
			</td>
			<td>
			    <%# aspnetforum.Utils.Topic.GetTopicInfoBMessageyID(Eval("LastMessageID"), Eval("Subject"), Eval("RepliesCount") as int?, Cmd) %>
			</td>
		</tr>
	</ItemTemplate>
	<FooterTemplate>
		</tbody></table>
	</FooterTemplate>
</asp:repeater>


</asp:Content>
