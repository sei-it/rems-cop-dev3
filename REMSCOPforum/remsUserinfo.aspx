﻿<%@ Page Title="" Language="C#" MasterPageFile="~/REMSCOPforum/AspNetForumMaster.Master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="remsUserinfo.aspx.cs" Inherits="REMSCOPforum_remsUserinfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHEAD" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="AspNetForumContentPlaceHolder" Runat="Server">

<div class="grid_12 omega">
	<section class="pad-top">
    <asp:Button ID="btnReport" runat="server" onclick="ExportExcel" Text="Export report" />
	</section>

<section class="pad-top">
    <asp:GridView ID="grdvwlist" runat="server" AutoGenerateColumns="False" 
         DataSourceID="SqlDataSource2" 
        AllowPaging="True"      
        onpageindexchanging="grdvwlist_PageIndexChanging" 
        onsorting="grdvwlist_Sorting">
        
       
        <Columns>
       
            <asp:BoundField DataField="aspnet_UserName" HeaderText="User Name" SortExpression="aspnet_UserName" />
            <asp:BoundField DataField="institutionType" HeaderText="Institution Type" SortExpression="institutionType" />
            <asp:BoundField DataField="k12Role" HeaderText="K12 Role" SortExpression="k12Role" />
            <asp:BoundField DataField="higherEDRole" HeaderText="Higher ED Role" SortExpression="higherEDRole" />
            <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
            <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
               
        </Columns>
       <%-- <FooterStyle BackColor="White" ForeColor="#000066" />
       <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
       <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
        <RowStyle ForeColor="#000066" />
        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#007DBB" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#00547E" /> --%>
    </asp:GridView>

    <asp:GridView ID="gvReports" runat="server" AutoGenerateColumns="False" Visible="False"
        DataKeyNames="Userid" DataSourceID="SqlDataSource1" >
        <Columns>
            <asp:BoundField DataField="Userid" HeaderText="User Id" ReadOnly="True" SortExpression="Userid" />
            <asp:BoundField DataField="aspnet_UserName" HeaderText="User Name" SortExpression="aspnet_UserName" />
            <asp:BoundField DataField="institutionType" HeaderText="Institution Type" SortExpression="institutionType" />
            <asp:BoundField DataField="k12Role" HeaderText="K12 Role" SortExpression="k12Role" />
            <asp:BoundField DataField="higherEDRole" HeaderText="Higher ED Role" SortExpression="higherEDRole" />
            <asp:BoundField DataField="otherRoleDes" HeaderText="Other RoleDes" SortExpression="otherRoleDes" />
            <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName" />
            <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
            <asp:BoundField DataField="State" HeaderText="State" SortExpression="State" /> 
            <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
            <asp:BoundField DataField="SelectedType" HeaderText="Selected Type" SortExpression="SelectedType" />
            <asp:BoundField DataField="yrExpEmergency" HeaderText="Yr Exp Emergency" SortExpression="yrExpEmergency" />
            <asp:BoundField DataField="yrExpED" HeaderText="Yr Exp ED" SortExpression="yrExpED" />
            <asp:BoundField DataField="interests" HeaderText="Interests" SortExpression="interests" />
            <asp:CheckBoxField DataField="isAccept" HeaderText="Is Accept" SortExpression="isAccept" />
            <asp:BoundField DataField="CreatedOn" HeaderText="Created On" SortExpression="CreatedOn" />
        </Columns>
        
<%--        <FooterStyle BackColor="White" ForeColor="#000066" />
        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
        <RowStyle ForeColor="#000066" />
        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#007DBB" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#00547E" />--%>
        
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:REMSforumConnectionString %>" 
        SelectCommand="SELECT [Userid], [aspnet_UserName], [institutionType], [k12Role], [higherEDRole], [otherRoleDes], [FirstName], [LastName], [State], [City], [Email], [SelectedType], [yrExpEmergency], [yrExpED], [interests], [isAccept], [CreatedOn] FROM [remsUsers]">
    </asp:SqlDataSource>
     <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:REMSforumConnectionString %>" 
        SelectCommand="SELECT [aspnet_UserName], [institutionType], [k12Role], [higherEDRole], [FirstName], [LastName], [Email] FROM [remsUsers]">
    </asp:SqlDataSource>
    </section>
    </div>
  
</asp:Content>

