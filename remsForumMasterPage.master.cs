﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class remsForumMasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string[] arrlist;
        string PageName, FolderName;
        arrlist = Request.Url.ToString().Split('/');

        if(arrlist.Length > 1)
        {
            PageName = arrlist[arrlist.Length - 1].ToString();
            FolderName = arrlist[arrlist.Length - 2].ToString();
        }
        else
        {
            PageName = arrlist[arrlist.Length - 1].ToString();
            FolderName = "";
        }
        PageName = PageName.Substring(0, PageName.IndexOf('.')).ToLower();

        ltlCOPnav.Controls.Add(new LiteralControl("<ul>"));

        if (Session["aspnetforumUserID"] == null)   //not log in  
        {
            coploginID.InnerText = "CoP Log in";
            switch (PageName)
            {
                case "default":
                    //home
                    ltlCOPnav.Controls.Add(new LiteralControl("<li class=\"active\">"));
                    Literal ltlHomeDefault = new Literal();
                    ltlHomeDefault.ID = "ltlHomeDefault";
                    ltlHomeDefault.Text = "CoP HOME";

                    ltlCOPnav.Controls.Add(ltlHomeDefault);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //about
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnHome1 = new LinkButton();
                    lnkbtnHome1.ID = "lnkbtnHome1";
                    lnkbtnHome1.PostBackUrl = "~/2_About.aspx";
                    lnkbtnHome1.Text = "ABOUT THE CoP";
                    lnkbtnHome1.CausesValidation = false;
                    ltlCOPnav.Controls.Add(lnkbtnHome1);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //rules
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnHome2 = new LinkButton();
                    lnkbtnHome2.CausesValidation = false;
                    lnkbtnHome2.ID = "lnkbtnHome2";
                    lnkbtnHome2.PostBackUrl = "~/3_PortalRules.aspx";
                    lnkbtnHome2.Text = "RULES";
                    ltlCOPnav.Controls.Add(lnkbtnHome2);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //join
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnHome3 = new LinkButton();
                    lnkbtnHome3.ID = "lnkbtnHome3";
                    lnkbtnHome3.CausesValidation = false;
                    lnkbtnHome3.PostBackUrl = "~/Account/Register.aspx";
                    lnkbtnHome3.Text = "JOIN";
                    ltlCOPnav.Controls.Add(lnkbtnHome3);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //log in
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnHome4 = new LinkButton();
                    lnkbtnHome4.ID = "lnkbtnHome4";
                    lnkbtnHome4.CausesValidation = false;
                    lnkbtnHome4.PostBackUrl = "~/Account/Login.aspx?ReturnUrl=../REMSCOPforum/COPindex.aspx";
                    lnkbtnHome4.Text = "<span class=\"glyphicon glyphicon-log-in\"></span> LOG IN";
                    ltlCOPnav.Controls.Add(lnkbtnHome4);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    break;
                case "2_about":
                    //home
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnAbout1 = new LinkButton();
                    lnkbtnAbout1.ID = "lnkbtnAbout1";
                    lnkbtnAbout1.CausesValidation = false;
                    lnkbtnAbout1.PostBackUrl = "~/default.aspx";
                    lnkbtnAbout1.Text = "CoP HOME";
                    ltlCOPnav.Controls.Add(lnkbtnAbout1);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //about
                    ltlCOPnav.Controls.Add(new LiteralControl("<li class=\"active\">"));
                    Literal ltlHomeAbout = new Literal();
                    ltlHomeAbout.ID = "ltlHomeAbout";
                    lnkbtnAbout1.CausesValidation = false;
                    ltlHomeAbout.Text = "ABOUT THE CoP";
                    ltlCOPnav.Controls.Add(ltlHomeAbout);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));

                    //rules
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnAbout2 = new LinkButton();
                    lnkbtnAbout2.ID = "lnkbtnAbout2";
                    lnkbtnAbout2.CausesValidation = false;
                    lnkbtnAbout2.PostBackUrl = "~/3_PortalRules.aspx";
                    lnkbtnAbout2.Text = "RULES";
                    ltlCOPnav.Controls.Add(lnkbtnAbout2);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //join
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnAbout3 = new LinkButton();
                    lnkbtnAbout3.ID = "lnkbtnAbout3";
                    lnkbtnAbout3.CausesValidation = false;
                    lnkbtnAbout3.PostBackUrl = "~/Account/Register.aspx";
                    lnkbtnAbout3.Text = "JOIN";
                    ltlCOPnav.Controls.Add(lnkbtnAbout3);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //log in
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnAbout4 = new LinkButton();
                    lnkbtnAbout4.ID = "lnkbtnAbout4";
                    lnkbtnAbout4.CausesValidation = false;
                    lnkbtnAbout4.PostBackUrl = "~/Account/Login.aspx?ReturnUrl=../REMSCOPforum/COPindex.aspx";
                    lnkbtnAbout4.Text = "<span class=\"glyphicon glyphicon-log-in\"></span> LOG IN";
                    ltlCOPnav.Controls.Add(lnkbtnAbout4);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    break;
                case "3_portalrules":
                    //home
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnRules1 = new LinkButton();
                    lnkbtnRules1.ID = "lnkbtnRules1";
                    lnkbtnRules1.CausesValidation = false;
                    lnkbtnRules1.PostBackUrl = "~/default.aspx";
                    lnkbtnRules1.Text = "CoP HOME";
                    ltlCOPnav.Controls.Add(lnkbtnRules1);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //about
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnRules2 = new LinkButton();
                    lnkbtnRules2.ID = "lnkbtnRules2";
                    lnkbtnRules2.CausesValidation = false;
                    lnkbtnRules2.PostBackUrl = "~/2_About.aspx";
                    lnkbtnRules2.Text = "ABOUT THE CoP";
                    ltlCOPnav.Controls.Add(lnkbtnRules2);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //rulse
                    ltlCOPnav.Controls.Add(new LiteralControl("<li class=\"active\">"));
                    Literal ltlHomeRules = new Literal();
                    ltlHomeRules.ID = "ltlHomeRules";

                    ltlHomeRules.Text = "RULES";
                    ltlCOPnav.Controls.Add(ltlHomeRules);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));

                    //join
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnRules3 = new LinkButton();
                    lnkbtnRules3.ID = "lnkbtnRules3";
                    lnkbtnRules3.CausesValidation = false;
                    lnkbtnRules3.PostBackUrl = "~/Account/Register.aspx";
                    lnkbtnRules3.Text = "JOIN";
                    ltlCOPnav.Controls.Add(lnkbtnRules3);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //log in
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnRules4 = new LinkButton();
                    lnkbtnRules4.ID = "lnkbtnRules4";
                    lnkbtnRules4.CausesValidation = false;
                    lnkbtnRules4.PostBackUrl = "~/Account/Login.aspx?ReturnUrl=../REMSCOPforum/COPindex.aspx";
                    lnkbtnRules4.Text = "<span class=\"glyphicon glyphicon-log-in\"></span> LOG IN ";
                    ltlCOPnav.Controls.Add(lnkbtnRules4);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    break;

                case "register":
                    //home
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnJoin1 = new LinkButton();
                    lnkbtnJoin1.ID = "lnkbtnJoin1";
                    lnkbtnJoin1.PostBackUrl = "~/default.aspx";
                    lnkbtnJoin1.Text = "CoP HOME";
                    lnkbtnJoin1.CausesValidation = false;
                    ltlCOPnav.Controls.Add(lnkbtnJoin1);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //about
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnJoin2 = new LinkButton();
                    lnkbtnJoin2.ID = "lnkbtnJoin2";
                    lnkbtnJoin2.CausesValidation = false;
                    lnkbtnJoin2.PostBackUrl = "~/2_About.aspx";
                    lnkbtnJoin2.Text = "ABOUT THE CoP";
                    ltlCOPnav.Controls.Add(lnkbtnJoin2);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //rulse
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnJoin3 = new LinkButton();
                    lnkbtnJoin3.ID = "lnkbtnJoin3";
                    lnkbtnJoin3.PostBackUrl = "~/3_PortalRules.aspx";
                    lnkbtnJoin3.CausesValidation = false;
                    lnkbtnJoin3.Text = "RULES";
                    ltlCOPnav.Controls.Add(lnkbtnJoin3);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //join
                    ltlCOPnav.Controls.Add(new LiteralControl("<li class=\"active\">"));
                    Literal ltlHomeJoin = new Literal();
                    ltlHomeJoin.ID = "ltlHomeJoin";
                    ltlHomeJoin.Text = "JOIN";
                    ltlCOPnav.Controls.Add(ltlHomeJoin);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //log in
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnJoin4 = new LinkButton();
                    lnkbtnJoin4.ID = "lnkbtnJoin4";
                    lnkbtnJoin4.CausesValidation = false;
                    lnkbtnJoin4.PostBackUrl = "~/Account/Login.aspx?ReturnUrl=../REMSCOPforum/COPindex.aspx";
                    lnkbtnJoin4.Text = "<span class=\"glyphicon glyphicon-log-in\"></span> LOG IN";
                    ltlCOPnav.Controls.Add(lnkbtnJoin4);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    break;

                default:  //log in
                    //home
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnLogin1 = new LinkButton();
                    lnkbtnLogin1.ID = "lnkbtnLogin1";
                    lnkbtnLogin1.CausesValidation = false;
                    lnkbtnLogin1.PostBackUrl = "~/Default.aspx";
                    lnkbtnLogin1.Text = "CoP HOME";
                    ltlCOPnav.Controls.Add(lnkbtnLogin1);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //about
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnLogin2 = new LinkButton();
                    lnkbtnLogin2.ID = "lnkbtnLogin2";
                    lnkbtnLogin2.CausesValidation = false;
                    lnkbtnLogin2.PostBackUrl = "~/2_About.aspx";
                    lnkbtnLogin2.Text = "ABOUT THE CoP";
                    ltlCOPnav.Controls.Add(lnkbtnLogin2);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //rulse
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnLogin3 = new LinkButton();
                    lnkbtnLogin3.ID = "lnkbtnLogin3";
                    lnkbtnLogin3.CausesValidation = false;
                    lnkbtnLogin3.PostBackUrl = "~/3_PortalRules.aspx";
                    lnkbtnLogin3.Text = "RULES";
                    ltlCOPnav.Controls.Add(lnkbtnLogin3);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //join
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnLogin4 = new LinkButton();
                    lnkbtnLogin4.ID = "lnkbtnLogin4";
                    lnkbtnLogin4.CausesValidation = false;
                    lnkbtnLogin4.PostBackUrl = "~/Account/Register.aspx";
                    lnkbtnLogin4.Text = "JOIN";
                    ltlCOPnav.Controls.Add(lnkbtnLogin4);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //log in
                    ltlCOPnav.Controls.Add(new LiteralControl("<li class=\"active\">"));
                    Literal ltlHomeLogin = new Literal();
                    ltlHomeLogin.ID = "ltlHomeLogin";

                    ltlHomeLogin.Text = "<span class=\"glyphicon glyphicon-log-in\"></span> LOG IN";
                    ltlCOPnav.Controls.Add(ltlHomeLogin);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    break;
            }

        }
        else  //log in already
        {
            coploginID.InnerText = "CoP Log out";
            coploginID.HRef = "~/Logout.aspx";
            switch (PageName)
            {
                case "default":
                    if (FolderName == "REMSCOPforum")
                    {
                        //home
                        ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                        LinkButton lnkbtnHomehome = new LinkButton();
                        lnkbtnHomehome.ID = "lnkbtnHomehome";
                        lnkbtnHomehome.CausesValidation = false;
                        lnkbtnHomehome.PostBackUrl = "~/default.aspx";
                        lnkbtnHomehome.Text = "CoP HOME";
                        ltlCOPnav.Controls.Add(lnkbtnHomehome);
                        ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                        //about
                        ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                        LinkButton lnkbtnHome1 = new LinkButton();
                        lnkbtnHome1.ID = "lnkbtnHome1";
                        lnkbtnHome1.CausesValidation = false;
                        lnkbtnHome1.PostBackUrl = "~/2_About.aspx";
                        lnkbtnHome1.Text = "ABOUT THE CoP";
                        ltlCOPnav.Controls.Add(lnkbtnHome1);
                        ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                        //forums
                        ltlCOPnav.Controls.Add(new LiteralControl("<li class=\"active\">"));
                        Literal ltlHomeDefault = new Literal();
                        ltlHomeDefault.ID = "ltlHomeDefault";

                        ltlHomeDefault.Text = "FORUMS";
                        ltlCOPnav.Controls.Add(ltlHomeDefault);
                        ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                        //rules
                        ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                        LinkButton lnkbtnHome2 = new LinkButton();
                        lnkbtnHome2.ID = "lnkbtnHome2";
                        lnkbtnHome2.CausesValidation = false;
                        lnkbtnHome2.PostBackUrl = "~/3_PortalRules.aspx";
                        lnkbtnHome2.Text = "RULES";
                        ltlCOPnav.Controls.Add(lnkbtnHome2);
                        ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                        //My profile
                        ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                        LinkButton lnkbtnHome3 = new LinkButton();
                        lnkbtnHome3.ID = "lnkbtnHome3";
                        lnkbtnHome3.CausesValidation = false;
                        lnkbtnHome3.PostBackUrl = "~/REMSCOPforum/editprofile.aspx";
                        lnkbtnHome3.Text = "MY PROFILE";
                        ltlCOPnav.Controls.Add(lnkbtnHome3);
                        ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                        //log out
                        ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                        LinkButton lnkbtnHome4 = new LinkButton();
                        lnkbtnHome4.ID = "lnkbtnHome4";
                        lnkbtnHome4.CausesValidation = false;
                        lnkbtnHome4.Click += new EventHandler(Logout);
                        lnkbtnHome4.Text = "<span class=\"glyphicon glyphicon-log-out\"></span> LOG OUT";
                        ltlCOPnav.Controls.Add(lnkbtnHome4);
                        ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    }
                    else
                    {
                        //home
                        ltlCOPnav.Controls.Add(new LiteralControl("<li class=\"active\">"));
                        Literal ltlHomeDefault = new Literal();
                        ltlHomeDefault.ID = "ltlHomeDefault";

                        ltlHomeDefault.Text = "CoP HOME";
                        ltlCOPnav.Controls.Add(ltlHomeDefault);
                        ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                        //about
                        ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                        LinkButton lnkbtnHome1 = new LinkButton();
                        lnkbtnHome1.ID = "lnkbtnHome1";
                        lnkbtnHome1.CausesValidation = false;
                        lnkbtnHome1.PostBackUrl = "~/2_About.aspx";
                        lnkbtnHome1.Text = "ABOUT THE CoP";
                        ltlCOPnav.Controls.Add(lnkbtnHome1);
                        ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                        //forums
                        ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                        LinkButton lnkbtnHomeForum = new LinkButton();
                        lnkbtnHomeForum.ID = "lnkbtnHomeForum";
                        lnkbtnHomeForum.CausesValidation = false;
                        lnkbtnHomeForum.PostBackUrl = "~/REMSCOPforum/default.aspx";
                        lnkbtnHomeForum.Text = "FORUMS";
                        ltlCOPnav.Controls.Add(lnkbtnHomeForum);
                        ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                        //rules
                        ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                        LinkButton lnkbtnHome2 = new LinkButton();
                        lnkbtnHome2.ID = "lnkbtnHome2";
                        lnkbtnHome2.CausesValidation = false;
                        lnkbtnHome2.PostBackUrl = "~/3_PortalRules.aspx";
                        lnkbtnHome2.Text = "RULES";
                        ltlCOPnav.Controls.Add(lnkbtnHome2);
                        ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                        //My profile
                        ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                        LinkButton lnkbtnHome3 = new LinkButton();
                        lnkbtnHome3.ID = "lnkbtnHome3";
                        lnkbtnHome3.CausesValidation = false;
                        lnkbtnHome3.PostBackUrl = "~/REMSCOPforum/editprofile.aspx";
                        lnkbtnHome3.Text = "MY PROFILE";
                        ltlCOPnav.Controls.Add(lnkbtnHome3);
                        ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                        //log out
                        ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                        LinkButton lnkbtnHome4 = new LinkButton();
                        lnkbtnHome4.ID = "lnkbtnHome4";
                        lnkbtnHome4.CausesValidation = false;
                        lnkbtnHome4.Click += new EventHandler(Logout);
                        lnkbtnHome4.Text = "<span class=\"glyphicon glyphicon-log-out\"></span> LOG OUT";
                        ltlCOPnav.Controls.Add(lnkbtnHome4);
                        ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    }
                    break;
                case "2_about":
                    //home
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnAbout1 = new LinkButton();
                    lnkbtnAbout1.ID = "lnkbtnAbout1";
                    lnkbtnAbout1.CausesValidation = false;
                    lnkbtnAbout1.PostBackUrl = "~/default.aspx";
                    lnkbtnAbout1.Text = "CoP HOME";
                    ltlCOPnav.Controls.Add(lnkbtnAbout1);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //about
                    ltlCOPnav.Controls.Add(new LiteralControl("<li class=\"active\">"));
                    Literal ltlHomeAbout = new Literal();
                    ltlHomeAbout.ID = "ltlHomeAbout";

                    ltlHomeAbout.Text = "ABOUT THE CoP";
                    ltlCOPnav.Controls.Add(ltlHomeAbout);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //Forums
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnAboutforum = new LinkButton();
                    lnkbtnAboutforum.CausesValidation = false;
                    lnkbtnAboutforum.ID = "lnkbtnAboutforum";
                    lnkbtnAboutforum.PostBackUrl = "~/REMSCOPforum/default.aspx";
                    lnkbtnAboutforum.Text = "FORUMS";
                    ltlCOPnav.Controls.Add(lnkbtnAboutforum);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //rules
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnAbout2 = new LinkButton();
                    lnkbtnAbout2.ID = "lnkbtnAbout2";
                    lnkbtnAbout2.CausesValidation = false;
                    lnkbtnAbout2.PostBackUrl = "~/3_PortalRules.aspx";
                    lnkbtnAbout2.Text = "RULES";
                    ltlCOPnav.Controls.Add(lnkbtnAbout2);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //My Profile
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnAbout3 = new LinkButton();
                    lnkbtnAbout3.ID = "lnkbtnAbout3";
                    lnkbtnAbout3.CausesValidation = false;
                    lnkbtnAbout3.PostBackUrl = "~/REMSCOPforum/editprofile.aspx";
                    lnkbtnAbout3.Text = "MY PROFILE";
                    ltlCOPnav.Controls.Add(lnkbtnAbout3);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));



                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnAbout4a = new LinkButton();
                    lnkbtnAbout4a.ID = "lnkbtnAbout4a";
                    lnkbtnAbout4a.CausesValidation = false;
                    lnkbtnAbout4a.PostBackUrl = "~/logout.aspx";
                    lnkbtnAbout4a.Text = "<span class=\"glyphicon glyphicon-log-out\"></span> LOG OUT";
                    ltlCOPnav.Controls.Add(lnkbtnAbout4a);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));

                    //log out
//                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
//                    Button lnkbtnAbout4 = new Button();
//                    lnkbtnAbout4.ID = "lnkbtnAbout4";
//                    lnkbtnAbout4.Click += new EventHandler(Logout);
//                    //lnkbtnAbout4.Text = "<span class=\"glyphicon glyphicon-log-out\"></span> LOG OUT";
//                    //lnkbtnAbout4.Text = "<span class=\"glyphicon glyphicon-log-out\">LOG after x OUT</span> ";
//lnkbtnAbout4.Text = "LOG after x OUT  ";
//                    ltlCOPnav.Controls.Add(lnkbtnAbout4);
//                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    break;
                case "3_portalrules":
                    //home
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnRules1 = new LinkButton();
                    lnkbtnRules1.ID = "lnkbtnRules1";
                    lnkbtnRules1.CausesValidation = false;
                    lnkbtnRules1.PostBackUrl = "~/default.aspx";
                    lnkbtnRules1.Text = "CoP HOME";
                    ltlCOPnav.Controls.Add(lnkbtnRules1);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //about
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnRules2 = new LinkButton();
                    lnkbtnRules2.ID = "lnkbtnRules2";
                    lnkbtnRules2.CausesValidation = false;
                    lnkbtnRules2.PostBackUrl = "~/2_About.aspx";
                    lnkbtnRules2.Text = "ABOUT THE CoP";
                    ltlCOPnav.Controls.Add(lnkbtnRules2);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //forums
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnRulesforums = new LinkButton();
                    lnkbtnRulesforums.ID = "lnkbtnRulesforums";
                    lnkbtnRulesforums.CausesValidation = false;
                    lnkbtnRulesforums.PostBackUrl = "~/REMSCOPforum/default.aspx";
                    lnkbtnRulesforums.Text = "FORUMS";
                    ltlCOPnav.Controls.Add(lnkbtnRulesforums);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //rulse
                    ltlCOPnav.Controls.Add(new LiteralControl("<li class=\"active\">"));
                    Literal ltlHomeRules = new Literal();
                    ltlHomeRules.ID = "ltlHomeRules";

                    ltlHomeRules.Text = "RULES";
                    ltlCOPnav.Controls.Add(ltlHomeRules);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));

                    //My profile
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnRules3 = new LinkButton();
                    lnkbtnRules3.ID = "lnkbtnRules3";
                    lnkbtnRules3.CausesValidation = false;
                    lnkbtnRules3.PostBackUrl = "~/REMSCOPforum/editprofile.aspx";
                    lnkbtnRules3.Text = "MY PROFILE";
                    ltlCOPnav.Controls.Add(lnkbtnRules3);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //log out
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnRules4 = new LinkButton();
                    lnkbtnRules4.ID = "lnkbtnRules4";
                    lnkbtnRules4.CausesValidation = false;
                    lnkbtnRules4.Click += new EventHandler(Logout);
                    lnkbtnRules4.Text = "<span class=\"glyphicon glyphicon-log-out\"></span> LOG OUT";
                    ltlCOPnav.Controls.Add(lnkbtnRules4);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    break;

                case "register":
                    //home
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnJoin1 = new LinkButton();
                    lnkbtnJoin1.ID = "lnkbtnJoin1";
                    lnkbtnJoin1.CausesValidation = false;
                    lnkbtnJoin1.PostBackUrl = "~/default.aspx";
                    lnkbtnJoin1.Text = "CoP HOME";
                    ltlCOPnav.Controls.Add(lnkbtnJoin1);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //about
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnJoin2 = new LinkButton();
                    lnkbtnJoin2.ID = "lnkbtnJoin2";
                    lnkbtnJoin2.CausesValidation = false;
                    lnkbtnJoin2.PostBackUrl = "~/2_About.aspx";
                    lnkbtnJoin2.Text = "ABOUT THE CoP";
                    ltlCOPnav.Controls.Add(lnkbtnJoin2);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //forums
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnJoinforums = new LinkButton();
                    lnkbtnJoinforums.ID = "lnkbtnJoinforums";
                    lnkbtnJoinforums.CausesValidation = false;
                    lnkbtnJoinforums.PostBackUrl = "~/REMSCOPforum/default.aspx";
                    lnkbtnJoinforums.Text = "FORUMS";
                    ltlCOPnav.Controls.Add(lnkbtnJoinforums);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));

                    //rulse
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnJoin3 = new LinkButton();
                    lnkbtnJoin3.ID = "lnkbtnJoin3";
                    lnkbtnJoin3.CausesValidation = false;
                    lnkbtnJoin3.PostBackUrl = "~/3_PortalRules.aspx";
                    lnkbtnJoin3.Text = "RULES";
                    ltlCOPnav.Controls.Add(lnkbtnJoin3);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //my profile
                    ltlCOPnav.Controls.Add(new LiteralControl("<li class=\"active\">"));
                    Literal ltlHomemyProfile = new Literal();
                    ltlHomemyProfile.ID = "ltlHomemyProfile";

                    ltlHomemyProfile.Text = "MY PROFILE";
                    ltlCOPnav.Controls.Add(ltlHomemyProfile);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //log out
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnJoin4 = new LinkButton();
                    lnkbtnJoin4.ID = "lnkbtnJoin4";
                    lnkbtnJoin4.CausesValidation = false;
                    lnkbtnJoin4.Click += new EventHandler(Logout);
                    lnkbtnJoin4.Text = "<span class=\"glyphicon glyphicon-log-out\"></span> LOG OUT";
                    ltlCOPnav.Controls.Add(lnkbtnJoin4);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    break;

                default:  //log in
                    //home
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnLogin1 = new LinkButton();
                    lnkbtnLogin1.ID = "lnkbtnLogin1";
                    lnkbtnLogin1.PostBackUrl = "~/Default.aspx";
                    lnkbtnLogin1.CausesValidation = false;
                    lnkbtnLogin1.Text = "CoP HOME";
                    ltlCOPnav.Controls.Add(lnkbtnLogin1);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //about
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnLogin2 = new LinkButton();
                    lnkbtnLogin2.ID = "lnkbtnLogin2";
                    lnkbtnLogin2.CausesValidation = false;
                    lnkbtnLogin2.PostBackUrl = "~/2_About.aspx";
                    lnkbtnLogin2.Text = "ABOUT THE CoP";
                    ltlCOPnav.Controls.Add(lnkbtnLogin2);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //forums
                    ltlCOPnav.Controls.Add(new LiteralControl("<li class=\"active\">"));
                    Literal ltlHomeforums = new Literal();
                    ltlHomeforums.ID = "ltlHomemyProfile";

                    ltlHomeforums.Text = "FORUMS";
                    ltlCOPnav.Controls.Add(ltlHomeforums);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //rulse
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnLogin3 = new LinkButton();
                    lnkbtnLogin3.ID = "lnkbtnLogin3";
                    lnkbtnLogin3.CausesValidation = false;
                    lnkbtnLogin3.PostBackUrl = "~/3_PortalRules.aspx";
                    lnkbtnLogin3.Text = "RULES";
                    ltlCOPnav.Controls.Add(lnkbtnLogin3);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //My profile
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnLogin4 = new LinkButton();
                    lnkbtnLogin4.ID = "lnkbtnLogin4";
                    lnkbtnLogin4.CausesValidation = false;
                    lnkbtnLogin4.PostBackUrl = "~/REMSCOPforum/editprofile.aspx";
                    lnkbtnLogin4.Text = "MY PROFILE";
                    ltlCOPnav.Controls.Add(lnkbtnLogin4);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    //log out
                    ltlCOPnav.Controls.Add(new LiteralControl("<li>"));
                    LinkButton lnkbtnJoinlogout = new LinkButton();
                    lnkbtnJoinlogout.ID = "lnkbtnJoin4";
                    lnkbtnJoinlogout.CausesValidation = false;
                    lnkbtnJoinlogout.Click += new EventHandler(Logout);
                    lnkbtnJoinlogout.Text = "<span class=\"glyphicon glyphicon-log-out\"></span> LOG OUT";
                    ltlCOPnav.Controls.Add(lnkbtnJoinlogout);
                    ltlCOPnav.Controls.Add(new LiteralControl("</li>"));
                    break;
            }
        }

        ltlCOPnav.Controls.Add(new LiteralControl("</ul>"));
    }

    protected void Logout(object sender, EventArgs e)
    {
        Session["aspnetforumUserID"] = null;
        Session.Abandon();
        FormsAuthentication.SignOut();

        Response.Redirect("~/default.aspx");
        return;
    }
}
