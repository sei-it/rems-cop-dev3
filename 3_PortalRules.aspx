﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsForumMasterPage.master" AutoEventWireup="true" CodeFile="3_PortalRules.aspx.cs" Inherits="_3_PortalRules" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">

    <div class="remsCOP-content">
    <div class="rcontainer">
    <div class="container ad">
 <div class="grid_12 omega">  
    <h2>
REMS TA Center Community Rules
    </h2>
    <p>
        The REMS TA Center Community of Practice (CoP) is a virtual space for schools, school districts, institutions of higher education (IHEs), and their community partners to collaborate on special projects, share news and resources, discuss trends and ideas, and learn from the experiences of others in the field. As a forum for practitioners in the field, content on the REMS TA Center CoP might come from various sources and might be based on the experiences of CoP members and nonmembers. Statements, opinions, resources, or any other submissions posted to the CoP by a member are not necessarily endorsed by the U.S. Department of Education (ED) or the REMS TA Center. ED and the REMS TA Center do not support the selling of products, services, or goods by vendors in the CoP. 
    </p>
    <p>
       Please remember that the community will reflect and be enhanced by the diversity of those who support our nation’s schools and IHEs. At times, community members might not share the same opinion on a topic. Our Codes of Conduct and Terms of Use are designed to ensure that all community members receive respect from their fellow members while participating. Please read and accept our Codes of Conduct and Terms of Use to ensure a productive and healthy experience for all community members. 
    </p>
    <h3 class="left">CODES OF CONDUCT:</h3>
    <ul class="general">
        <li><strong>Think twice before posting confidential or sensitive information</strong>. Our goal is to ensure the privacy and security of all CoP members. However, the REMS TA Center cannot guarantee that knowledge and information exchanged will not be shared outside the CoP. Protect yourself and others, and refrain from posting sensitive information that could cause harm if viewed outside of the CoP.</li>
        <li><strong>You are accountable for messages posted to the CoP</strong>. To ensure that you (and not your school, district, IHE, or agency) are responsible for content posted, we require that all members use their real name and official affiliation.</li>
        <li><strong>A strong and diverse community requires mutual respect</strong>. The REMS TA Center encourages CoP members to participate in forum discussions and debates. We ask that the opinions and thoughts of all members be respected. Please also consider that the CoP is open to a diverse audience, and some members might not be communicating in their primary language.</li>
    </ul>
    <h3 class="left">TERMS OF USE:</h3>
    <ol class="general">
        <li>ED and the REMS TA Center do not guarantee or warrant that any information posted by individuals on these pages is correct and disclaim any liability for any loss or damage resulting from reliance on any such information. ED and the REMS TA Center may not be able to verify, do not warrant or guarantee, and assume no liability for anything posted on this website by any other person. ED and the REMS TA Center do not endorse, support, or otherwise promote any private or commercial entity or the information, products, or services contained on those Web sites that may be reached through links on our Web site. Opinions and comments that appear in the CoP belong to the individuals who expressed them. They do not belong to or represent views of ED or the REMS TA Center.</li>    
        <li>These pages may not be used for the submission of any claim, demand, informal or formal complaint, or any other form of legal and/or administrative notice or process, or for the exhaustion of any legal and/or administrative remedy.</li>  
        <li>REMS TA Center CoP members are solely responsible for ensuring that they do not act in any manner that constitutes or forms a part of a course of conduct amounting to a violation of any state, federal, international, or other applicable law. This includes, but is not limited to, posting of content in violation of the copyright on that content.</li>  
        <li>The REMS TA Center does not allow content that is abusive, vulgar, racist, sexist, slanderous, harassing, misleading, or otherwise objectionable. We also do not allow content that promotes the selling of products, goods, and services. Content of this nature will be screened and removed. </li>  
    </ol>
    <h3 class="left">
        TERMINATION:
    </h3>
    <p>
        The U.S. Department of Education and REMS TA Center may terminate or suspend your access to all or part of the REMS TA Center CoP, including but not limited to any discussion forums on its site, for any reason, including breach of the Terms of Use or Codes of Conduct. If you are unsatisfied with the services provided by the REMS TA Center, please email <a href="mailto:info@remstacenter.org">info@remstacenter.org</a> to terminate your membership.
    </p>
    <h3 class="left">PRIVACY POLICY:</h3>
    <p>
The U.S. Department of Education and REMS TA Center will not sell any personal information about REMS TA Center CoP members to any third parties. The U.S. Department of Education and REMS TA Center may use account information generically in order to revise its services or present findings from the use of its services. 
</p>

  </div>  
    
    </div>
</div>
</div>

    
</asp:Content>

