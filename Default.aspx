﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsForumMasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
    <!--BEGIN MAIN CONTENT -->
			<div class="remsCOP-content">
				<!--BEGIN FIRST SECTION-->
				<section>
					<div class="container row clearfix">
						<div class="container ad">
							<div class="grid_12 omega">
								<h2>Where Partners in School and Higher Ed Safety<br>
					      Come Together</h2> </div>
							<!-- /grid_12 omega -->
							<div class="grid_12 omega">
								<p>The REMS TA Center CoP is a collaborative of practitioners with the collective aim to enhance the ability of schools, school districts, IHEs, state education agencies (SEAs), and their community partners to develop high-quality emergency operations plans (EOPs) and implement comprehensive emergency management planning efforts through the sharing of ideas, experiences, lessons learned, and by engaging with one another on these important topics.</p>
							</div>
							<!-- /grid_12 omega -->
							<div class="grid_12 omega">
								<div class="tabs-container">
									<!-- Nav tabs -->
									<ul class="nav nav-tabs" role="tablist">
										<li role="presentation" class="active"><a href="#collaborate" aria-controls="collaborate" role="tab" data-toggle="tab"><span class="fa fa-cogs"></span> Collaborate</a></li>
										<li role="presentation"><a href="#share" aria-controls="share" role="tab" data-toggle="tab"><span class="fa fa-comments-o"></span> Share</a></li>
										<li role="presentation"><a href="#getinspired" aria-controls="getinspired" role="tab" data-toggle="tab"><span class="fa fa-lightbulb-o"></span> Get Inspired</a></li>
										<li role="presentation"><a href="#support" aria-controls="support" role="tab" data-toggle="tab"><span class="fa fa-life-bouy"></span> Get Support</a></li>
									</ul>
									<!-- Tab panes -->
									<div class="tab-content" id="copTabs">
										<div role="tabpanel" class="tab-pane active" id="collaborate">
											<p>Create or join a <a href="REMSCOPforum/default.aspx?gid=12">community huddle</a> to initiate conversations and topics specific to smaller, more focused groups within the nationwide CoP.</p>
										</div>
										<div role="tabpanel" class="tab-pane" id="share">
											<p> Share success stories and safety tips based on real-world experiences with <a href="REMSCOPforum/default.aspx?gid=13">school</a> and <a href="REMSCOPforum/default.aspx?gid=13">IHE</a> emergencies, and offer recommendations for recovery efforts. </p>
										</div>
										<div role="tabpanel" class="tab-pane" id="getinspired">
											<p><a href="http://rems.ed.gov/REMSPublications.aspx" target="_blank">View lessons learned and see what other schools and IHEs are doing.</a></p>
										</div>
										<div role="tabpanel" class="tab-pane" id="support">
											<p>Chat with <a href="mailto:info@remstacenter.org">REMS TA Center staff</a> or request FREE technical assistance in a variety of emergency management areas.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="grid_12 omega">
								<p class="pad-top">Anyone involved in the field of school and/or IHE emergency management is welcome to join, from members of emergency operations planning teams and law enforcement officers to community members and parents.</p>
							</div>
							<div class="grid_12 omega"> <a id="joinID" runat="server" class="btn btn-info btn-lg" href="Account/Register.aspx"><i class="fa fa-user-plus"></i> Join</a> <a class="btn btn-info btn-lg" href="2_About.aspx"><i class="fa fa-info-circle"></i> Learn More</a> <a id="loginID" runat="server" class="btn btn-info btn-lg" href="Account/Login.aspx"><i class="fa fa-sign-in"></i> Log In</a> </div>						</div>
						<!-- /container -->
					</div>
					<!-- /container row CoP clearfix -->
				</section>
				<!-- END FIRST SECTION -->
				<!-- BEGIN TRENDING TOPICS-->
				<section class="trending-topics">
					<div class="container row clearfix">
						<div class="container ad">
							<div class="grid_12 omega">
								<h2>Trending Topics</h2> </div>
							<div class="grid_12 omega">
								<p>A sneak peek of some of the topics currently being discussed in our forums. <span id="extraTextID" runat="server">There are many categories and topics. <a href="Account/Register.aspx">Sign up</a> to see more!</span></p>
							</div>
							<div class="grid_12 omega">
								<div class="carousel-wrapper">
									<div id="trendingTopics" class="carousel slide" data-ride="carousel">
										<!-- Indicators -->
										<ol class="carousel-indicators">
											<li data-target="#trendingTopics" data-slide-to="0" class="active"></li>
											<li data-target="#trendingTopics" data-slide-to="1"></li>
											<li data-target="#trendingTopics" data-slide-to="2"></li>
											<li data-target="#trendingTopics" data-slide-to="3"></li>
											<li data-target="#trendingTopics" data-slide-to="4"></li>
										</ol>
										<!-- Wrapper for slides -->
										<div class="carousel-inner" role="listbox">
                                        
                                        
										<div class="item active">
												<div class="topics-box">
													<h3>America’s PrepareAthon!</h3>
													<div class="grid_3"> <img src="images/K12-pop-icon.png" class="img-responsive" alt="K-12 icon" /> </div>
													<div class="grid_8">
														<p>Share how you’ve participated in this national campaign in April, September, and year-round.</p>
														<div class="grid_12 omega"> <a class="btn btn-info btn-lg" href="http://rems.ed.gov/COP/REMSCOPforum/topics.aspx?ForumID=86">Read More <i class="fa fa-angle-double-right"></i></a> </div>
													</div>
												</div>
											</div>                                        
                                        
                                        
                                        
                                        
										<div class="item">
												<div class="topics-box">
													<h3>IHE Emergency Management Resources</h3>
													<div class="grid_3"> <img src="images/IHE-pop-icon-1.png" class="img-responsive" alt="IHE icon" /> </div>
													<div class="grid_8">
														<p>Visit this forum for up-to-date resources from the REMS TA Center, U.S. Department of Education, and Federal partners in higher ed safety. </p>
													</div>
													<div class="grid_12 omega"> <a class="btn btn-info btn-lg" href="http://rems.ed.gov/COP/REMSCOPforum/topics.aspx?ForumID=73">Read More <i class="fa fa-angle-double-right"></i></a> </div>
												</div>
											</div>
                                            
                                            
                                            
										<div class="item">
												<div class="topics-box">
													<h3>EOP ASSIST Discussion Forum</h3>
													<div class="grid_3"><img src="images/K12-pop-icon.png" class="img-responsive" alt="K-12 icon" /> </div>
													<div class="grid_8">
														<p>A forum for EOP ASSIST users to share feedback, exchange ideas, and pose questions.</p>
													</div>
													<div class="grid_12 omega"> <a class="btn btn-info btn-lg" href="http://rems.ed.gov/COP/REMSCOPforum/topics.aspx?ForumID=85">Read More <i class="fa fa-angle-double-right"></i></a> </div>
												</div>
											</div>                                            
                                            
										<div class="item">
												<div class="topics-box">
													<h3>K-12 Emergency Management Resources</h3>
													<div class="grid_3"> <img src="images/K12-pop-icon.png" class="img-responsive" alt="K-12 icon" /> </div>
													<div class="grid_8">
														<p>Visit this forum for up-to-date resources from the REMS TA Center, U.S. Department of Education, and Federal partners in school safety.</p>
													</div>
													<div class="grid_12 omega"> <a class="btn btn-info btn-lg" href="http://rems.ed.gov/COP/REMSCOPforum/topics.aspx?ForumID=72">Read More <i class="fa fa-angle-double-right"></i></a> </div>
												</div>
											</div>  
                                                                                      
											<div class="item">
												<div class="topics-box">
													<h3>Developing High-Quality IHE EOPs—Lessons Learned</h3>
													<div class="grid_3"> <img src="images/IHE-pop-icon-1.png" class="img-responsive" alt="IHE icon" /> </div>
													<div class="grid_8">
														<p>Designed to facilitate discussions among members of the IHE community about common questions, issues, and trends related to emergency management and developing emergency operations plans (EOPs). Click to start a new topic or join a conversation.</p>
													</div>
													<div class="grid_12 omega"> <a class="btn btn-info btn-lg" href="http://rems.ed.gov/COP/REMSCOPforum/topics.aspx?ForumID=22">Read More <i class="fa fa-angle-double-right"></i></a> </div>
												</div>
											</div>                                        
                                        
                                        
 
										</div>
										<!-- Left and right controls -->
										<a class="left carousel-control" href="#trendingTopics" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
										<a class="right carousel-control" href="#trendingTopics" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- END TRENDING TOPICS -->
				<!-- BEGIN COMMUNITY SPOTLIGHT -->
				<section class="community-spotlight">
					<div class="container row clearfix">
						<div class="container ad">
							<div class="grid_12 omega">
								<h2>Community Spotlights</h2> </div>
							<div class="grid_12 omega">
								<div class="carousel-wrapper">
									<div id="communitySpotlight" class="carousel slide" data-ride="carousel">
										<!-- Indicators -->
										<ol class="carousel-indicators">
											<li data-target="#communitySpotlight" data-slide-to="0" class="active"></li>
											<li data-target="#communitySpotlight" data-slide-to="1"></li>
											<li data-target="#tcommunitySpotlight" data-slide-to="2"></li>
											<li data-target="#communitySpotlight" data-slide-to="3"></li>
											<li data-target="#communitySpotlight" data-slide-to="4"></li>
										</ol>
										<!-- Wrapper for slides -->
										<div class="carousel-inner" role="listbox">
											<div class="item active">
												<div class="spotlight-box">
													
													<div class="grid_12">
														<div class="spotlight-text">
															<h3>EOP ASSIST Updates and Events in September 2015</h3>
                                                            <div class="grid_12">
                                                            <div class="grid_3"> <img src="images/eop-assist.jpg" class="img-responsive" alt="EOP Assist logo" /> </div>
															<div class="grid_8"><p>To help school administrators and emergency management personnel create or revise their emergency operations plan (EOPs), the REMS TA Center released a free, Web-accessible software application, EOP ASSIST, in January 2015.</p>
                                                             <div class="grid_12 omega">
                                                            <a class="btn btn-info btn-lg" href="EOP_ASSIST.aspx">Read More <i class="fa fa-angle-double-right"></i></a>
                                                            </div>
                                                            </div>
                                                            </div>

													</div>
												</div>
											</div>
                                            </div>
											<div class="item">
												<div class="spotlight-box">
													
													<div class="grid_12">
														<div class="spotlight-text">
															<h3>TO BE RELEASED Emergency Management Virtual Toolkit</h3>
                                                            
                                                            <div class="grid_12">
                                                            <div class="grid_3">
                                                            <img src="images/virtual-toolkit.jpg" class="img-responsive" alt="Virtual Toolkit" />
                                                            </div>
                                                            <div class="grid_8">
															<p>Stay tuned for the release of a virtual toolkit that state education agencies (SEAs) and their partners (e.g., emergency management agencies, state departments of health) around the country can store on their state sites.</p>
                                                            <div class="grid_12 omega">
                                                            <a class="btn btn-info btn-lg" href="Virtual_Toolkit.aspx">Read More <i class="fa fa-angle-double-right"></i></a>
                                                            </div>
                                                            </div>
                                                            </div>
													</div>
												</div>
											</div>
                                            </div>
                                            
											<div class="item">
												<div class="spotlight-box">
													
													<div class="grid_12">
														<div class="spotlight-text">
															<h3>TO BE RELEASED Teen CERT Toolkit</h3>
                                                            <div class="grid_3">
                                                            <img src="images/teen-cert.jpg" class="img-responsive" alt="Teen Cert logo" />
                                                            </div>
                                                            <div class="grid_8">
															<p>Stay tuned for the release of this print-based toolkit, which will include everything a school needs to develop and implement a Teen CERT program, including PowerPoint presentations for principals, course descriptions, permission forms, classroom training supplies, and more.</p>
                                                            <div class="grid_12">
                                                            <a class="btn btn-info btn-lg" href="CERT_Toolkit.aspx">Read More <i class="fa fa-angle-double-right"></i></a>
                                                            </div>
													</div>
												</div>
											</div>
                                            </div>
                                            </div>
                                            
											<div class="item">
												<div class="spotlight-box">
													
													<div class="grid_12">
														<div class="spotlight-text">
															<h3>We’ve Joined Twitter! Follow us @REMTACENTER</h3>
                                                            <div class="grid_3">
                                                            <span class="fa fa-twitter-square"></span>
                                                            </div>
                                                            <div class="grid_8">
															<p>You can now visit us at <a href="https://twitter.com/search?q=%40remstacenter">@remstacenter</a> to get up-to-date access to our site updates, tool launches, publication releases, training opportunities, and much more.</p>
                                                            <div class="grid_12">
                                                            <a class="btn btn-info btn-lg" href="REMTACENTER.aspx">Read More <i class="fa fa-angle-double-right"></i></a>
                                                            </div>
                                                            </div>
													</div>
												</div>
											</div>
											</div>
											<div class="btn-wrapper"> <span aria-hidden="true"><a class="fa fa-arrow-circle-left" id="backBtn"></a></span> <span aria-hidden="true"><a class="fa fa-arrow-circle-right" id="fwdBtn"></a></span> </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- END COMMUNITY SPOTLIGHT -->
			</div>
	<!--END MAIN CONTENT-->
    
    
    <script type="text/javascript">
        function ShowMessage() { alert("To start your own Community Huddle, email us at info@remstacenter.org with a suggested title, description and list of invited participants."); }
    
    </script>    
    
</asp:Content>

