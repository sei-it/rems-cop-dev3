﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsForumMasterPage.master" AutoEventWireup="true" CodeFile="Virtual_Toolkit.aspx.cs" Inherits="Virtual_Toolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
<div class="remsCOP-content">
    <div class="rcontainer">
    <div class="container ad">
 <div class="grid_12 omega"> 
 <h2>TO BE RELEASED Emergency Management Virtual Toolkit</h2>
 <div class="grid_12 omega">
 <img src="images/virtual-toolkit.jpg" class="img-responsive articleImg" alt="Virtual Toolkit" />
 <p>Stay tuned for the release of a virtual toolkit that state education agencies (SEAs) and their partners (e.g., emergency management agencies, state departments of health) around the country can store on their state sites. School districts may be interested in the toolkit as well. The toolkit will feature up-to-date resources, information, and training opportunities from the REMS TA Center, the U.S. Department of Education and other Federal agencies. It will be synchronized so that those entities who opt to include it as a part of their Website can feel confident that they are providing the most current information. We can’t wait to share it when it’s live! If you’re interested in receiving a copy for your SEA, please e-mail us at <a href="mailto:info@remstacenter.org">info@remstacenter.org</a> under the Emergency Management Virtual Toolkit.</p>
 </div>
 </div>
 </div>
 </div>
 </div>




</asp:Content>

