﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class events : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DateTime today = DateTime.Now;
            CalendarControl1.Month = today.Month;
            CalendarControl1.Year = today.Year;
            CalendarControl1.GroupID = 0;
        }
    }
}