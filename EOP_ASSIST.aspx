﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsForumMasterPage.master" AutoEventWireup="true" CodeFile="EOP_ASSIST.aspx.cs" Inherits="EOP_ASSIST" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
   <div class="remsCOP-content">
    <div class="rcontainer">
    <div class="container ad">
 <div class="grid_12 omega"> 
 <h2>EOP ASSIST Updates and Events in September 2015</h2>
 <div class="grid_12 omega">
 <img src="images/eop-assist.jpg" class="img-responsive articleImg" alt="EOP Assist logo" />
 <p>To help school administrators and emergency management personnel create or revise their emergency operations plan (EOPs), the REMS TA Center released a free, Web-accessible software application, EOP ASSIST, in January 2015. This user-friendly application walks school and district emergency management personnel through the six-step planning process for creating a customized school EOP recommended in the Federal Guide for Developing High-Quality School Emergency Operations Plans. Since January, more than 350 practitioners have downloaded this application.</p>
 
  <p>Based on the feedback we received from users in the field since its launch, the TA Center released a major update to EOP ASSIST in September 2015 that will:</p>
</div>
 <div class="grid_12 omega">

<ul class="general">
<li>Allow for state-level hosting and state administrator functions.</li>
<li>Increase management capabilities of school districts over the management of school EOPs</li>
<li>EEnhance the compatibility of EOP ASSIST with server software (both MySQL and SQL Server).</li>
<li>Streamline the planning process with an improved user interface that allows for easier plan formatting options.</li>
</ul>

<p>The update will be available for download at <a href="http://rems.ed.gov/EOPAssist.aspx">http://rems.ed.gov/EOPAssist.aspx</a>Please note that this update is compatible with previously downloaded versions of the application. In other words, any plan content developed with an earlier version of EOP ASSIST will be maintained with this update.</p>

<p>Also by request from the field, the REMS TA Center is pleased to announce the release of the <i>EOP ASSIST Interactive Workbook</i>. This workbook will contain the same content as EOP ASSIST in a Microsoft Word format and will be available to any users who prefer to use an offline version of this tool. The workbook will be available for download from the REMS TA Center Website, or is available via jump drives.</p>

<p>Finally, the REMS TA Center hosted an EOP ASSIST Webinar on September 22 and an EOP ASSIST virtual meeting on September 24. The Webinar showcased the features and functions of EOP ASSIST and provide information about the new updates to EOP ASSIST. The virtual meeting allowed practitioners to pose questions and discuss EOP ASSIST with each other and with the EOP ASSIST team from the Office of Safe and Healthy Students and the REMS TA Center. For more information on these events, please visit <a href="http://rems.ed.gov/TA_Webinars.aspx">http://rems.ed.gov/TA_Webinars.aspx</a>.</p>


 </div>
 </div>
 </div>
 </div>
 </div>

</asp:Content>

