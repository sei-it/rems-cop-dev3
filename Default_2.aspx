﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsForumMasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentLeftMenu" Runat="Server">
    <div class="column left forumleft">
<h4 class="sideMenu">Community of Practice</h4>
<ul>
<li><asp:Label ID="lblid1" Width="225" ForeColor="#85952A" runat="server"><strong>Home</strong></asp:Label></li>
<li><asp:LinkButton ID="LinkButton2" PostBackUrl="~/2_About.aspx" runat="server">About the REMS TA Center CoP</asp:LinkButton></li>
<li><asp:LinkButton ID="LinkButton4" PostBackUrl="~/3_PortalRules.aspx" runat="server">Community Rules</asp:LinkButton></li>
<li><asp:LinkButton ID="LinkButton3" PostBackUrl="~/Account/Register.aspx" runat="server">Join the Community</asp:LinkButton></li>
<li><asp:LinkButton ID="LinkButton5" PostBackUrl="~/Account/Login.aspx?ReturnUrl=../REMSCOPforum/default.aspx" runat="server">Log in</asp:LinkButton></li>
<li><asp:LinkButton ID="LinkButton1" onClientClick="window.open('events.aspx');" runat="server">Events</asp:LinkButton></li>
</ul>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">
    <div class="main3x">
    <div id="whatis">
    	<h2 class="green">What is the REMS TA Center CoP? </h2>
        <div class="greenarr"><img src="images/green_arrowDOWN.png"></div>
  <p>A virtual space for schools, school districts, institutions of higher education (IHEs), and their community partners to collaborate on special projects, share news and resources, discuss trends and ideas, and learn from the experiences of others in the field.</p>
      
    </div>
    <div id="coplogin">
    	<ul class="coplogin">
        	<li><a href="#" title="" class="btnCop">Join</a></li>
           <li><a href="#" title="" class="btnCop">Start A Group</a></li>
           <li><a href="#" title="" class="btnCop">Enter A Forum</a></li>
           <li><a href="#" title="" class="btnCop">My Profile</a></li>
           <li class="lastli"><a href="#" title="" class="btnCop">Login/out</a></li>
        </ul>
        <div class="clear"></div>
    </div>
</div>


<div class="columnSmall right"> 
				<div class="featureBox">
						<h3 class="featureHead icon_cal">
							Who Should Join?</h3>
					<ul>
                  <li>Seasoned Practitioners Addressing Emergency Management at Schools, Districts or IHEs</li>
                  <li>K-12 Administrators, Educators, Principals, or Senior-Level Officials</li>
                  <li>IHE Presidents, Chancellors, or Provosts</li>
                  <li>School Resource Officers or Law Enforcement Officers Working With a School, District, or IHE</li>
                  <li>First Responders</li>
                  <li>Counselors or Psychologists</li>
                  <li>Parents</li>
                  </ul>
				</div>

				<div class="featureBox">
						<h3 class="featureHead icon_cal copFeature">
							How Can You Contribute and Connect?</h3>
					<ul>
        <li>Share lessons learned, see what other schools and IHEs are doing, and ask questions.</li>
        <li>View resources, ask questions, share your concerns, and contribute ideas to improve emergency operations plan (EOP) development and processes before, during, and after emergency events.</li>
        <li>Share your experiences, discuss processes and regulations, and offer advice to others in the field.</li>
        <li>Share success stories and safety tips based on real-world experiences with school emergencies.</li>
        <li>Offer recommendations for psychological recovery efforts.</li>
    </ul>
			   </div>
    </div> <!-- / column right -->

</asp:Content>













