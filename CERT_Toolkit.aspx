﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsForumMasterPage.master" AutoEventWireup="true" CodeFile="CERT_Toolkit.aspx.cs" Inherits="CERT_Toolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
<div class="remsCOP-content">
    <div class="rcontainer">
    <div class="container ad">
 <div class="grid_12 omega"> 
 <h2>TO BE RELEASED Teen CERT Toolkit</h2>
 <div class="grid_12 omega">
 <img src="images/teen-cert.jpg" class="img-responsive articleImg" alt="Teen Cert logo" />
 <p>Stay tuned for the release of this print-based toolkit, which will include everything a school needs to develop and implement a Teen CERT program, including PowerPoint presentations for principals, course descriptions, permission forms, classroom training supplies, and more.</p>
 <p>The toolkit will include details for administrators and classroom teachers. For a preview of the toolkit pre-launch, view the resources provided as a supplement to the Teen CERT: Enhancing School Emergency Management Through Youth Engagement and Preparedness Webinar we held in May in collaboration with the Federal Emergency Management Agency’s Individual and Community Preparedness Division and Susan Graves, Safety Coordinator for Lincoln County School District in Oregon.</p> 
 
 </div>

 </div>
 </div>
 </div>
 </div>
</asp:Content>

