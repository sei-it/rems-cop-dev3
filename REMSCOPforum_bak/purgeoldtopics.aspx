﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="purgeoldtopics.aspx.cs" EnableEventValidation="false" Inherits="aspnetforum.purgeoldtopics" MasterPageFile="AspNetForumMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolderHEAD" runat="server">
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/themes/ui-lightness/jquery-ui.css" type="text/css" rel="Stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.js" type="text/javascript"></script>
    <link href="../css/calendar.css" rel="stylesheet" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="AspNetForumContentPlaceHolder">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

<p><asp:Label ID="lblPurge" runat="server" EnableViewState="False" meta:resourcekey="lblEditForumResource1">Purge old topics</asp:Label></p>

<asp:TextBox id="tbDateFrom" CssClass="datepick" runat="server" meta:resourcekey="tbDateFromResource1"  />
<asp:Button ID="btnPurge" runat="server" Text="Purge topics older than this" OnClick="btnPurge_Click" UseSubmitBehavior="false" />
 <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="tbDateFrom"
     ErrorMessage="This field is required!"></asp:RequiredFieldValidator>
     <asp:RegularExpressionValidator ID="RegExpVldDate" runat="server" 
     ErrorMessage="Datetime is not well formated." ForeColor="Red" ControlToValidate="tbDateFrom" 
     ValidationExpression="^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$"></asp:RegularExpressionValidator> 

<ajax:CalendarExtender ID="cldextDate" runat="server" Format="MM/dd/yyyy" TargetControlID="tbDateFrom"  CssClass="AjaxCalendar" />
<asp:Label runat="server" ID="lblRes"></asp:Label>

<%--datepicker--%>
<script type="text/javascript">
	$(function () {
		$(".datepick").datepicker({ gotoCurrent: true, dateFormat: "yy-mm-dd", firstDay: 1 });
	});
</script>


</asp:Content>