﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["aspnetforumUserID"] == null)
        {
            joinID.Visible = true;
            loginID.InnerText = "Log In";
            loginID.HRef = "~/Account/Login.aspx";
            extraTextID.Visible = true;
            //ltlLoginout.Text = "Log In";
            //lbtnloginout.Text = "Log In";
            //lbtnCOPindex.Visible = false;
        }
        else
        {
            joinID.Visible = false;
            loginID.InnerText = "Log Out";
            loginID.HRef = "~/Logout.aspx";
            extraTextID.Visible = false;
            //lbtnloginout.Text = "Log Out";
            //ltlLoginout.Text = "Log Out";
            //lbtnCOPindex.Visible = true;
        }
    }
}