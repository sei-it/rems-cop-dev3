﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CalendarControl.ascx.cs"
    Inherits="EVEA.project.CalendarControl" %>
<table style="border-collapse: collapse; width: 100%; margin-bottom: 0;">
    <tr class="cldTblRow">
        <td>
            <asp:LinkButton ID="LinkButton1" runat="server" Text="" OnClick="OnPreviousMonth"><img border="0" alt="back image" src="images/back.gif" /></asp:LinkButton>
            &nbsp;&nbsp;
            <asp:Label ID="lblMonth" runat="server"></asp:Label>
            &nbsp;&nbsp;
            <asp:LinkButton ID="LinkButton2" runat="server" Text="" OnClick="OnNextMonth"><img border="0" alt="forward image" src="images/forward.gif" /></asp:LinkButton>
        </td>
    </tr>
</table>
<div id="CalendarDiv"  class="calendarWrap" runat="server">
</div>
