﻿<%@ Page Title="" Language="C#" MasterPageFile="~/remsForumMasterPage.master" AutoEventWireup="true" CodeFile="2_About.aspx.cs" Inherits="_2_About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentMain" Runat="Server">

   <div class="remsCOP-content">
    <div class="rcontainer">
    <div class="container ad">
 <div class="grid_12 omega">  


    <h2>
       About the REMS TA Center Community of Practice
    </h2>
    
    
    <p>
The REMS TA Center Community of Practice (CoP) is a virtual space for representatives from schools, school districts, institutions of higher education (IHEs), and their community partners to collaborate on special projects, share news and resources, discuss trends and ideas, and learn from the experiences of others in the field. 
    </p>
    
    
    <ul class="general">
        <li><b>What is the purpose?</b> To create a collaborative community that enhances the ability of schools, IHEs, and their community partners to develop high-quality emergency operations plans (EOPs) and implement comprehensive emergency management planning efforts.  </li>
        
        <li><b>Who should join? </b>Anyone working in the field of school and/or IHE emergency management, from members of emergency operations planning teams and law enforcement officers to community members, and parents.</li>
        
        <li>
            <b>Are there any requirements to join?</b> Yes. We value respectful and constructive collaboration. <a href="3_PortalRules.aspx" >View our Community Rules.</a>
        </li>
    </ul>
    
    
    <p>All REMS TA Center CoP members can collaborate and interact via our topic-based forums. Explore our <a href="REMSCOPforum/default.aspx">forums</a> to learn more.</p>
    <p>After joining, members can start community huddles to facilitate more focused discussions, collaborate on special projects, or streamline access to local content and conversations. </p>
    <p>Explore our <a href="REMSCOPforum/default.aspx?gid=12">community huddles</a> to learn more. </p>
   <!-- <p>Explore our <a href="Account/Login.aspx">groups</a> to learn more.</p>-->
    
  </div>  
    
    </div>
</div>
</div>

 

</asp:Content>

